package com.example.validaremailandpassword.Adapters

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.example.validaremailandpassword.model.ListItems
import com.example.validaremailandpassword.databinding.ListUserBinding
//import kotlinx.android.synthetic.main.list_user.view.*
import java.util.Collections.addAll

//var context: Context? = null
//var view2 : View? = null

private lateinit var userBinding: ListUserBinding

class AdapterLista(var userList: ArrayList<ListItems>, val context: Context) : RecyclerView.Adapter<AdapterLista.ViewHolder>() {
    private var filteredUserList: ArrayList<ListItems>? = null
    var clickAdapter = true
    //interfaz para que realice funciones cuando se le de click a un item de la lista
    interface  OnUserClickListener{
        fun onItemClick()
    }


    //este método devuelve la vista para cada elemento de la lista
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_user, parent, false)
        val binding = ListUserBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        userBinding = ListUserBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(userBinding)
    }

    //este método vincula los datos de la lista
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemData= userList.get(position)


//        if (itemData.rolUI.equals())

        if (itemData.rolUI.equals("usuario")){
            userBinding.listaNombre.text = itemData.nombre
            userBinding.listaApellido.text = itemData.apellido
            userBinding.listaCorreo.text = itemData.email
//            holder.itemView.listaNombre.text= itemData.nombre
//            holder.listaApellido.text = itemData.apellido
//            holder.listaCorreo.text = itemData.email

//            holder.itemView.listnombre.setText(itemData.nombre)
//            holder.itemView.listaApellido.setText(itemData.apellido)
//            holder.itemView.listaCorreo.setText(itemData.email)
        }else{
            Log.e("TAG", "onBindViewHolder: administrador --> $itemData")
            holder.itemView.visibility = View.INVISIBLE

        }


//        holder.bindItems(userList[position])


        //cuando se le de click a un item
        /*holder.itemView.setOnClickListener {

            val dialog = AlertDialog.Builder(context).create()
            val view = LayoutInflater.from(context).inflate(R.layout.activity_custom_dialog_admin_list, null)


            var nombre : TextView
            var apellido : TextView
            var edad: TextView
            var sexo: TextView
            var telefono: TextView
            nombre = view.findViewById(R.id.infoNombre)
            apellido = view.findViewById(R.id.infoApellido)
            edad = view.findViewById(R.id.infoEdad)
            sexo = view.findViewById(R.id.infoSexo)
            telefono = view.findViewById(R.id.infoTelefono)
            nombre.text = itemData.nombre
            apellido.text = itemData.apellido
            edad.text = itemData.edad.toString()
            sexo.text = itemData.sexo
            telefono.text = itemData.telefono

            dialog.setView(view)
            dialog.setCancelable(true)
            dialog.setOnDismissListener {
                view.isEnabled = true
            }
            dialog.show()
            dialog.window!!.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)
        }*/
    }

    //este método está dando el tamaño de la lista
    override fun getItemCount(): Int {
        return userList.size
    }


    // la clase está manejando la vista de lista
    class ViewHolder(userBinding : ListUserBinding) : RecyclerView.ViewHolder(userBinding.root) {

//
//        fun bindItems(user: ListItems) {
//            val textViewName = itemView.findViewById(R.id.listaNombre) as TextView
//            val textViewAddress  = itemView.findViewById(R.id.listaApellido) as TextView
//            val textViewEmail  = itemView.findViewById(R.id.listaCorreo) as TextView
//            textViewName.text = user.name
//            textViewAddress.text = user.Lastname
//            textViewEmail.text = user.email
//
//
//            }
//
    }

    fun updateList(list: ArrayList<ListItems> ){
        userList = list
        notifyDataSetChanged()
    }

   fun clear() {
        userList.clear()
    }

    fun addRowItem(arrayList: ArrayList<ListItems>){
//        AdapterLista(arrayList,context)
        this.userList.addAll(arrayList)
        addAll(userList.toMutableList<Any>() as ArrayList<Any>)
    }
    fun adapterClick(click:Boolean){
        clickAdapter = click
    }




}
package com.example.validaremailandpassword.Adapters

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.validaremailandpassword.view.Fragment.PestGrafica
import com.example.validaremailandpassword.view.Fragment.PestLista



class ViewPagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {
//    private lateinit var animatorX: ObjectAnimator
//    private var duracion: Long = 1000
//    val boton = R.id.botonFlotante
    private var state: Int = 0
    private var isFloatButtonHidden: Boolean= false
    private var position : Int = 0
    companion object{
        private const val ARG_OBJET = "object"
    }
    override fun getItemCount(): Int = 2 //3

    override fun createFragment(position: Int): Fragment {
        return when(position) {
            0 -> {
                println(position)
                return PestLista()
            }
            1 -> {
                println(position)
                return PestGrafica()
            }
//            2 -> {
//                println(position)
////                animation("x")
//                return PestMaps()
//            }
           else -> PestLista()
       }

        val fragment = PestLista()
        fragment.arguments = Bundle().apply {
            putInt(ARG_OBJET, position + 1)
        }
        return fragment


    }

}



//    private fun animation(animation: String) {
//        when(animation){
//            "x" -> {
//                animatorX = ObjectAnimator.ofFloat(boton, "x", 300f)
//                animatorX.duration = duracion
//                var animatorSetX: AnimatorSet = AnimatorSet()
//                animatorSetX.play(animatorX)
//                animatorSetX.start()
//
//            }
//        }
//
//    }
//

package com.example.validaremailandpassword.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.validaremailandpassword.databinding.ListUserBinding
import com.example.validaremailandpassword.model.ListItems

class AdapterNewList(): RecyclerView.Adapter<ItemViewHolder>() {

    lateinit var  context: Context
    private val itemList = mutableListOf<ListItems>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        context = parent.context
        val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
        val binding: ListUserBinding= ListUserBinding.inflate(layoutInflater,parent,false)

        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.setItem(itemList[position])

//        holder.itemView.setOnClickListener {
//            listener.itemSelect(itemList[position])
//        }
    }

    fun setItems(list: MutableList<ListItems>){
        itemList.clear()
        itemList.addAll(list)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return itemList.size
    }


}
package com.example.validaremailandpassword.Adapters


import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.validaremailandpassword.databinding.ItemroomBinding
import com.example.validaremailandpassword.data.UserRoom


private lateinit var userBinding: ItemroomBinding

class AdapterRoom(val userRoom: ArrayList<UserRoom>, val context: Context):RecyclerView.Adapter<AdapterRoom.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterRoom.ViewHolder {
        userBinding = ItemroomBinding.inflate(LayoutInflater.from(context), parent, false)
        return ViewHolder(userBinding)
    }

    override fun onBindViewHolder(holder: AdapterRoom.ViewHolder, position: Int) {
        val itemData= userRoom.get(position)
        userBinding.iditemroom.text = itemData.id.toString()
        userBinding.nameitemroom.text = itemData.nombre

    }

    override fun getItemCount(): Int {
        return userRoom.size
    }

    class ViewHolder(userBinding : ItemroomBinding) : RecyclerView.ViewHolder(userBinding.root) {

    }

    fun clear() {
        userRoom.clear()
    }

}

package com.example.validaremailandpassword.Adapters

import androidx.recyclerview.widget.RecyclerView
import com.example.validaremailandpassword.databinding.ListUserBinding
import com.example.validaremailandpassword.model.ListItems

class ItemViewHolder(binding: ListUserBinding):RecyclerView.ViewHolder(binding.root) {
    private var binding : ListUserBinding? = null


    init {
        this.binding = binding
    }

    fun setItem(model: ListItems){
        binding?.let { view ->
            view.listaCorreo.text = model.email
            view.listaApellido.text = model.apellido
            view.listaNombre.text = model.nombre

        }
    }
}
package com.example.validaremailandpassword.model

data class ListaModel(
        val Listas:MutableList<ListItems> = mutableListOf()
)

data class ListItems(
    val nombre: String,
    val apellido: String,
    val email: String,
    val edad: String,
    val sexo: String,
    val telefono: String,
    val password: String,
    val latitude: String,
    val longitude: String,
    val id: Int,
    val rolUI: String

)


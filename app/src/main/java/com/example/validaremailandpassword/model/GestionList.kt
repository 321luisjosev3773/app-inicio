package com.example.validaremailandpassword.model

data class GestionList (
        val nombre: String,
        val apellido: String,
        val telefono: String,
        val sexo:String,
        val edad: String,
        val rolUI:String,
        val latitude:String,
        val longitude:String,
        val email: String,
        val password:String
)

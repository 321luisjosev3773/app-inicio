package com.example.validaremailandpassword.view.Activity

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.lifecycle.ViewModelProviders
import com.example.validaremailandpassword.Api
import com.example.validaremailandpassword.ApiInterface
import com.example.validaremailandpassword.R
import com.example.validaremailandpassword.RetrofitInstance
import com.example.validaremailandpassword.di.UserViewModel
import com.example.validaremailandpassword.data.UserRoom
//import androidx.lifecycle.lifecycleScope
import com.example.validaremailandpassword.databinding.ActivityMainBinding
import com.example.validaremailandpassword.databinding.LoadingBinding
import com.example.validaremailandpassword.model.*
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.*

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


lateinit var progressBarLogin: ProgressDialog
lateinit var sharedPreferences: SharedPreferences
private lateinit var firebaseAnalytics: FirebaseAnalytics
//lateinit var preferences: SharedPreferences
lateinit var cardProgress: CardView
var isRemembered = false
private lateinit var botonRegistrate : Button;
var emails = ""
var passwords = ""

class MainActivity : AppCompatActivity() {
//    private val authUser:FirebaseAuth by lazy { FirebaseAuth.getInstance() }
    private val GOOGLE_SIGN_IN :Int= 10
//    private val callbackManager = CallbackManager.Factory.create()
    private lateinit var binding : ActivityMainBinding
    private lateinit var bdLoadingBinding: LoadingBinding
    private lateinit var userViewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {

        Thread.sleep(2000)
        setTheme(R.style.ValidarEmailAndPassword)

        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)

        //para guardar datos en cache y mantener la sesion activa
        sharedPreferences = getSharedPreferences("SHARED_PREF", Context.MODE_PRIVATE)

        Log.e("TAG", "check login: --->$isRemembered" )

        userViewModel = run {
            ViewModelProviders.of(this).get(UserViewModel::class.java)
        }

        isRemembered = sharedPreferences.getBoolean("check", false)
//        var checkCache = sharedPreferences.getBoolean("check" , true)
        var rol = sharedPreferences.getString("rolUI", "")

        if (isRemembered){


                when(rol){
                    "usuario" -> {
                        val intent= Intent(this, GestionUserActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                    "administrador" -> {
                        val intent= Intent(this, AdminActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                }



        }

        binding.emailLogin.onFocusChangeListener.apply {
            binding.emailLogin.isHintEnabled = false
        }
        binding.passwordLogin.onFocusChangeListener.apply {
            binding.passwordLogin.isHintEnabled = false
        }

        binding.botonLogin.setOnClickListener {
           validate()

        }

        binding.googleButton.setOnClickListener {
//            val providers = arrayListOf(
//                    AuthUI.IdpConfig.GoogleBuilder().build()
//            )
//
//            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestIdToken(getString(R.string.default_web_client_id))
//                .requestEmail()
//                .build()
//
//            val googleSignInClient = GoogleSignIn.getClient(this, gso)
//            val signInIntent = googleSignInClient.signInIntent
//            startActivityForResult(signInIntent , GOOGLE_SIGN_IN)


//            startActivityForResult(AuthUI.getInstance()
//                    .createSignInIntentBuilder()
//                    .setAvailableProviders(providers)
//                    .build(),GOOGLE_SIGN_IN)


//            val googleConf = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)//login por defecto
//                    .requestIdToken(getString(R.string.default_web_client_id))//token asociado a la app
//                    .requestEmail()
//                    .build()
//
//            val googleClient = GoogleSignIn.getClient(this, googleConf)//cliente de autenticacion de google

//            googleClient.signOut()
//            startActivityForResult(googleClient.signInIntent , GOOGLE_SIGN_IN)//mostrar la pantalla de autenticacion
        }

        binding.botonRegistrate.setOnClickListener {
            navigateToRegistro()
            finish()
        }

    }

    override fun onStart() {
        super.onStart()
        val account: GoogleSignInAccount? = GoogleSignIn.getLastSignedInAccount(this)
        Toast.makeText(this, "account ----> $account", Toast.LENGTH_LONG).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

//        if(requestCode == GOOGLE_SIGN_IN){
//            val response = IdpResponse.fromResultIntent(data)
//
//            if (resultCode == Activity.RESULT_OK){
//                val user = FirebaseAuth.getInstance().currentUser
//                startActivity(Intent(this, practicaMapa::class.java))
//
//            }else{
//                Toast.makeText(this, "error", Toast.LENGTH_LONG).show()
//            }
//        }
        Log.e("TAG", "onActivityResult: data--->$data --->$requestCode ---> $resultCode" )
        if (requestCode == GOOGLE_SIGN_IN){ // la respuesta esta correcta
            Log.e("TAG", "onActivityResult: --->paso " )

            val task = GoogleSignIn.getSignedInAccountFromIntent(data)

            Log.e("TAG", "onActivityResult: --->task $task" )
            try {
                val account = task.getResult(ApiException::class.java) //la cuenta de google
                Log.e("TAG", "onActivityResult: --->account $account" )
                if (account != null){
                    Log.e("TAG", "onActivityResult: -->paso2" )
                    val credential = GoogleAuthProvider.getCredential(account.idToken, null)
                    FirebaseAuth.getInstance().signInWithCredential(credential).addOnCompleteListener {
                        if (it.isSuccessful){
                            Log.e("TAG", "onActivityResult: --< todo bien" )
                            Toast.makeText(this,"todo bien", Toast.LENGTH_LONG).show()
                        var intent= Intent(this@MainActivity , GestionUserActivity::class.java)
//                        val editor : SharedPreferences.Editor = sharedPreferences.edit()
//                        editor.putBoolean("check", isRemembered)
//                        editor.putInt("id", )
//                        editor.putString("email", i.email)
//                        editor.putString("password", i.password)
//                        editor.putString("rolUI", i.rolUI)
//                        editor.apply()
//
                        startActivity(intent)
                        finish()
                        }else if(it.isComplete){

                            Toast.makeText(this, "completado", Toast.LENGTH_LONG).show()
                        }
                        else if (it.isCanceled){
                            Toast.makeText(this, "cancelado", Toast.LENGTH_LONG).show()
                        }else{
                            Toast.makeText(this, "error", Toast.LENGTH_LONG).show()
                        }
                    }

                }
            }catch (e:ApiException){
                Toast.makeText(this, "error con google", Toast.LENGTH_LONG).show()
            }



        }else{
            Toast.makeText(this, "algo salio mal", Toast.LENGTH_LONG).show()
        }

    }



    private  fun signin(emails: String, passwords:String){
        println("email signin --->$emails")
        println("password signin--->$passwords")



        val error = CoroutineExceptionHandler{coroutineContext, throwable ->
            Log.e("TAG", "error exepcion: ${throwable}" )
        }
        val corou = CoroutineScope(Dispatchers.IO)
                corou.launch(error) {
             Log.e("TAG", "onResponse: ${Thread.currentThread().name}")
                    RetrofitInstance.getRetrofitInstance()
                            .create(ApiInterface::class.java)
                            .getdato(emails, passwords)
                            .enqueue(object : Callback<List<ListItems>> {
                                     @SuppressLint("InvalidAnalyticsName")
                            override fun onResponse(call: Call<List<ListItems>>, response: Response<List<ListItems>>) {
                                Log.e("TAG", "onResponse: ${Thread.currentThread().name}")
                                if (response.isSuccessful) {
                                    Log.e("TAG", "dentro del if ${Thread.currentThread().name}: " )
                                    isRemembered = true
                                    if (response.body()!!.toString().length > 2) {
                                        Log.e("TAG", "onResponse: ${response.body().toString()}")
                                        Log.e("TAG", "onResponse: ${response.code()}")
                                        for (i in response.body()!!.listIterator()) {
                                            when (i.rolUI) {
                                                "usuario" -> {
                                                    progressBarLogin.dismiss()
                                                    when (i.telefono) {
                                                        null -> {
                                                            var intent = Intent(this@MainActivity, GestionUserActivity::class.java)
                                                            val editor: SharedPreferences.Editor = sharedPreferences.edit()
                                                            editor.putBoolean("check", isRemembered)
                                                            editor.putInt("id", i.id)
                                                            editor.putString("email", i.email)
                                                            editor.putString("password", i.password)
                                                            editor.putString("rolUI", i.rolUI)
                                                            editor.apply()
                                                            startActivity(intent)
                                                            finish()
                                                        }
                                                        else -> {


                                                            var intent = Intent(this@MainActivity, GestionUserActivity::class.java)
                                                            val editor: SharedPreferences.Editor = sharedPreferences.edit()
                                                            editor.putBoolean("check", isRemembered)
                                                            editor.putInt("id", i.id)
                                                            editor.putString("email", i.email)
                                                            editor.putString("password", i.password)
                                                            editor.putString("nombre", i.nombre)
                                                            editor.putString("apellido", i.apellido)
                                                            editor.putString("edad", i.edad)
                                                            editor.putString("sexo", i.sexo)
                                                            editor.putString("latitude", i.latitude)
                                                            editor.putString("longitude", i.longitude)
                                                            editor.putString("telefono", i.telefono)
                                                            editor.putString("rolUI", i.rolUI)
                                                            editor.apply()

                                                            userViewModel.saveUser(
                                                                UserRoom(i.id,i.nombre,i.apellido,i.email,i.edad
                                                                    ,i.sexo, i.telefono,i.password,i.latitude, i.longitude,i.rolUI))

                                                            startActivity(intent)
                                                            finish()
                                                        }
                                                    }
                                                }
                                                "administrador" -> {
                                                    progressBarLogin.dismiss()
                                                    firebaseAnalytics = Firebase.analytics
                                                    val bundle = Bundle()
                                                    bundle.putString("mensaje", "login admin")
                                                    firebaseAnalytics.logEvent("loginAdmin", bundle)
                                                    val intent = Intent(this@MainActivity, AdminActivity::class.java)
                                                    val editor: SharedPreferences.Editor = sharedPreferences.edit()
                                                    editor.putBoolean("check", isRemembered)
                                                    editor.putString("rolUI", i.rolUI)
                                                    editor.apply()
                                                    startActivity(intent)
                                                    finish()
                                                }
                                                else -> {
                                                    progressBarLogin.dismiss()
                                                    Toast.makeText(this@MainActivity, "no tiene rol", Toast.LENGTH_LONG).show()
                                                }
                                            }
                                        }
                                    } else {
                                        progressBarLogin.dismiss()
                                        Toast.makeText(this@MainActivity, "los datos no coinciden", Toast.LENGTH_SHORT).show()
                                    }
                                }else {
                                    progressBarLogin.dismiss()
                                    Log.e("TAG", "onResponse: \"request failed with ${response.code()}:${response.message()}")
                                    Toast.makeText(this@MainActivity, "inicio de sesion fallido", Toast.LENGTH_SHORT).show()
                                }
                            }

                            override fun onFailure(call: Call<List<ListItems>>, t: Throwable) {
                                Toast.makeText(
                                        this@MainActivity, "Fallo de conexion", Toast.LENGTH_SHORT
                                ).show()
                                progressBarLogin.dismiss()
                            }

                        })

        }


    }

    private fun navigateToRegistro() {
        val intent = Intent(this, RegistroActivity::class.java )
        startActivity(intent)
        finish()

    }


     fun validate(){
        val result = arrayOf(validateEmail(),validatePassword())
        if(false in result){
            return
        }else{
            bdLoadingBinding = LoadingBinding.inflate(LayoutInflater.from(this))
            progressBarLogin = ProgressDialog(this)
            progressBarLogin.setCanceledOnTouchOutside(false)
            progressBarLogin.show()
            progressBarLogin.setContentView(bdLoadingBinding.root)

            bdLoadingBinding.loadingText.text = "Cargando ..."

            progressBarLogin.window!!.setBackgroundDrawableResource(android.R.color.transparent)


//            layoutProgressbar.visibility = View.VISIBLE
            signin(emails, passwords)
        }
    }

    private fun validatePassword(): Boolean {
        val password= binding.passwordLogin.editText?.text.toString()

        return if(password.isEmpty()){
            binding.passwordLogin.error = "la contraseña esta vacio"
            binding.LCRR.setBackgroundResource(R.drawable.round_border_red)
            false
        }else{
            passwords = password
            binding.passwordLogin.error = null
            binding.LCRR.setBackgroundResource(R.drawable.round_border)
            true
        }
    }

    private fun validateEmail(): Boolean {
        val email= binding.emailLogin.editText?.text.toString()

       return if(email.isEmpty()){
                binding.emailLogin.error = "el correo esta vacio"
                binding.LCR.setBackgroundResource(R.drawable.round_border_red)
                false
            }else{
                 binding.LCR.setBackgroundResource(R.drawable.round_border)
                binding.emailLogin.error = null
                 emails = email
                true
               }
    }


//    private fun validate(){
//        val result = arrayOf(validateEmail(),validatorPassword())
//
//        if (false in result){
//            return
//        }else{
//            navigateToGestion()
//            Toast.makeText(this,"sesion iniciada", Toast.LENGTH_SHORT).show()
//        }
//    }
//
//
//    private fun validateEmail() : Boolean {
//        val email= binding.emailLogin.editText?.text.toString()
//
//        return when {
//            email.isEmpty() -> {
//                binding.emailLogin.error = "el correo esta vacio"
//                false
//            }
//            !PatternsCompat.EMAIL_ADDRESS.matcher(email).matches() -> {
//                binding.emailLogin.error = "Escribe un correo valido"
//                false
//            }
//            else -> {
//                binding.emailLogin.error = null
//                true
//            }
//        }
//    }
//
//    private fun validatorPassword(): Boolean {
//        val password= binding.passwordLogin.editText?.text.toString()
//        val passwordRegex =  Pattern.compile(
//                "^"+
//                        "(?=.*[0-9])"+      //por lo menos un numero
//                        "(?=.*[a-z])"+      //por lo menos una letra minuscula
//                        "(?=.*[A-Z])"+      //por lo menos una letra Mayuscula
//                        "(?=.*[@#$%^&+=])"+ //por lo menos un caracter especial
//                        "(?=\\S+$)"+         //no espacios en blanco
//                        ".{8,}"+            //por lo minimo 4 caracteres
//                        "$"
//
//        )
//        return when {
//            password.isEmpty() -> {
//                binding.passwordLogin.error = "Escribe una contraseña"
//                false
//            }
//            !passwordRegex.matcher(password).matches() -> {
//                binding.passwordLogin.error = "Mínimo 8 carácteres, una letra minúscula, una letra mayúscula, un carácter especial, sin espacios y un numero"
//                false
//            }
//            else -> {
//                binding.passwordLogin.error = null
//                true
//            }
//        }
//
//    }
}
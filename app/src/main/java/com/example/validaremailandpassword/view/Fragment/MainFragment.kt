package com.example.validaremailandpassword.view.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.validaremailandpassword.Adapters.ViewPagerAdapter
import com.example.validaremailandpassword.R
import com.example.validaremailandpassword.databinding.FragmentMainBinding
import com.google.android.material.tabs.TabLayoutMediator


class MainFragment : Fragment() {
    private val adapter by lazy { ViewPagerAdapter(requireActivity()) }
    private var mainBinding: FragmentMainBinding? = null
    private val binding get() = mainBinding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        mainBinding = FragmentMainBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.pager.adapter = adapter


        val tabLayoutMediator = TabLayoutMediator(binding.tabLayout, binding.pager,
            TabLayoutMediator.TabConfigurationStrategy{ tab, position ->
                when(position){
                    0 -> {
                        tab.text ="Usuarios"
//                    tab.setIcon(R.drawable.ic_list)
                    }
                    1 -> {
                        tab.text = "Estadisticas"
//                    tab.setIcon(R.drawable.ic_graph)
                    }

                }

            })
        tabLayoutMediator.attach()

    }

}
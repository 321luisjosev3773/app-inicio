package com.example.validaremailandpassword.view.Activity

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View.*
import android.widget.Toast
import androidx.core.util.PatternsCompat
import com.example.validaremailandpassword.R
import com.example.validaremailandpassword.model.SignUp
import com.example.validaremailandpassword.ApiInterface
import com.example.validaremailandpassword.RetrofitInstance
import com.example.validaremailandpassword.databinding.ActivityRegistroBinding
import com.example.validaremailandpassword.databinding.LoadingBinding
import com.google.firebase.auth.FirebaseAuth

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.util.regex.Pattern
import javax.security.auth.callback.Callback
lateinit var progressBar: ProgressDialog
var emaill: String = ""
var passwordd : String= ""
var rolUI: String = "usuario"

private lateinit var registroBinding: ActivityRegistroBinding
private lateinit var loadingBinding: LoadingBinding
class RegistroActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        registroBinding = ActivityRegistroBinding.inflate(layoutInflater)
        setContentView(registroBinding.root)

        registroBinding.botonAcceder.isClickable = true

        registroBinding.botonRegistrarse.setOnClickListener{
            validate()

        }


        registroBinding.botonAcceder.setOnClickListener {
            navigateToLogin()
        }
        registroBinding.emailRegistro.onFocusChangeListener.apply {
            registroBinding.emailRegistro.isHintEnabled = false
        }
        registroBinding.passwordRegistro.onFocusChangeListener.apply {
            registroBinding.passwordRegistro.isHintEnabled = false
        }
        registroBinding.passwordRegistro2.onFocusChangeListener.apply {
            registroBinding.passwordRegistro2.isHintEnabled = false
        }
    }



    fun navigateToLogin() {
        val intent = Intent(this, MainActivity::class.java )
        startActivity(intent)
        finish()

    }



private fun validate(){
    val result = arrayOf(validateEmail(),validatePassword(),validatePassword2())

    if (false in result){
        return
    }else{
//        Toast.makeText(this,"datos correctos", Toast.LENGTH_SHORT).show()
        loadingBinding = LoadingBinding.inflate(LayoutInflater.from(this))
        progressBar = ProgressDialog(this)
        progressBar.setCanceledOnTouchOutside(false)
        progressBar.show()
        progressBar.setContentView(loadingBinding.root)
        loadingBinding.loadingText.text = "Cargando registro..."

        progressBar.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        validarEmail(emaill)
    }
}

    private fun setup() {
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(emaill, passwordd).addOnCompleteListener {
            if(it.isSuccessful){
                
            }else{

            }
        }
    }

    private fun validateEmail() : Boolean {
    val email= registroBinding.emailRegistro.editText?.text.toString()

    return when {
        email.isEmpty() -> {
            registroBinding.emailRegistro.error = "el correo esta vacio"
            registroBinding.RCR.setBackgroundResource(R.drawable.round_border_red)
            false
        }
        !PatternsCompat.EMAIL_ADDRESS.matcher(email).matches() -> {
            registroBinding.emailRegistro.error = "Escribe un correo valido"
            registroBinding.RCR.setBackgroundResource(R.drawable.round_border_red)
            false
        }
        else -> {
            emaill = email
            println("email" + email )
            registroBinding.emailRegistro.error = null
            registroBinding.RCR.setBackgroundResource(R.drawable.round_border)
            true
        }
    }
}

private fun validatePassword(): Boolean {
    val password = registroBinding.passwordRegistro.editText?.text.toString()
    val passwordRegex = Pattern.compile(
        "^" +
                "(?=.*[0-9])" +      //por lo menos un numero
                "(?=.*[a-z])" +      //por lo menos una letra minuscula
                "(?=.*[A-Z])" +      //por lo menos una letra Mayuscula
                "(?=.*[@#$%^&+.=])" + //por lo menos un caracter especial
                "(?=\\S+$)" +         //no espacios en blanco
                ".{8,}" +            //por lo minimo 4 caracteres
                "$"

    )
    return when {
        password.isEmpty() -> {
            registroBinding.passwordRegistro.error = "Escribe una contraseña"
            registroBinding.RCRR.setBackgroundResource(R.drawable.round_border_red)
            false
        }

        !passwordRegex.matcher(password).matches() -> {
            registroBinding.passwordRegistro.error =
                "Mínimo 8 carácteres, una letra minúscula, una letra mayúscula, un carácter especial, sin espacios y un numero"
            registroBinding.RCRR.setBackgroundResource(R.drawable.round_border_red)
            false
        }
        else -> {
            registroBinding.passwordRegistro.error = null
            passwordd = password
            println("password" + passwordd)
            registroBinding.RCRR.setBackgroundResource(R.drawable.round_border)
            true
        }
    }
}

    private fun validatePassword2(): Boolean {
        val password = registroBinding.passwordRegistro.editText?.text.toString()
        val password2 = registroBinding.passwordRegistro2.editText?.text.toString()
        return if (password != password2) {
            registroBinding.passwordRegistro2.error = "La contraseña no coincide con la primera"
            registroBinding.RCRRR.setBackgroundResource(R.drawable.round_border_red)
            false
        } else {
            registroBinding.RCRRR.setBackgroundResource(R.drawable.round_border)
            true
        }
    }
    private fun validarEmail(emaill: String) {

        val retIn = RetrofitInstance.getRetrofitInstance().create(ApiInterface::class.java)
        retIn.getEmail(emaill).enqueue(object : retrofit2.Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Toast.makeText(
                        this@RegistroActivity, "Fallo de conexion", Toast.LENGTH_SHORT
                ).show()
                progressBar.dismiss()
            }
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.isSuccessful) {
                    val create = response.body()!!
                    val verificacion = create.string().length


                    if (verificacion.equals(2)) {
                        signup(emaill, passwordd, rolUI)
                        setup()

                    } else {
                        progressBar.dismiss()
                        Toast.makeText(applicationContext,"El email ya esta registrado", Toast.LENGTH_SHORT).show()
                    }


                } else {
                    progressBar.dismiss()
                    Log.e("TAG", "onResponse: \"request failed with ${response.code()}:${response.message()}")
                }
            }
        })

    }
    private fun signup(emaill: String,passwordd: String,rolUI: String){

        val retIn = RetrofitInstance.getRetrofitInstance().create(ApiInterface::class.java)
        val registerInfo = SignUp(emaill,passwordd,rolUI)
//        val registroInfo2 = SignUp("antonio@gmail.com", "123456789", "administrador")

        retIn.registerUser(registerInfo).enqueue(object : Callback, retrofit2.Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

                Toast.makeText(this@RegistroActivity, t.message, Toast.LENGTH_SHORT).show()

            }
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.code() == 201) {
                    progressBar.dismiss()
                    Toast.makeText(this@RegistroActivity, "Registro exitoso", Toast.LENGTH_SHORT).show()
                    registroBinding.botonAcceder.isClickable = false
                    navigateToLogin()
                }
                else{
                    progressBar.dismiss()
                    Toast.makeText(this@RegistroActivity, "Registro fallido", Toast.LENGTH_SHORT).show()

                }
            }
        })
    }


}



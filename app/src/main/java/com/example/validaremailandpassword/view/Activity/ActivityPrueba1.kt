package com.example.validaremailandpassword.view.Activity

import android.app.Activity
import android.content.Intent
import android.icu.text.SimpleDateFormat
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.validaremailandpassword.Adapters.AdapterLista
import com.example.validaremailandpassword.Adapters.AdapterRoom
import com.example.validaremailandpassword.R
import com.example.validaremailandpassword.databinding.ActivityPrueba1Binding
import com.example.validaremailandpassword.data.UserRoom
import com.example.validaremailandpassword.databinding.ActivityAdminBinding
import com.example.validaremailandpassword.databinding.ActivityAdminBinding.inflate
import com.example.validaremailandpassword.databinding.ActivityCustomDialogAdminListBinding
import com.example.validaremailandpassword.databinding.ExitBinding
import com.example.validaremailandpassword.di.UserViewModel
import com.example.validaremailandpassword.model.ListItems
import com.example.validaremailandpassword.onItemClick
import com.github.mikephil.charting.utils.Utils.init
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import java.util.*
import kotlin.collections.ArrayList

@Suppress("DEPRECATION")
class ActivityPrueba1 : AppCompatActivity() {
    private lateinit var userViewModel: UserViewModel
    private lateinit var binding: ActivityPrueba1Binding
    private lateinit var adminBinding: ActivityAdminBinding
    private lateinit var customDialogAdminListBinding: ActivityCustomDialogAdminListBinding
    private lateinit var listaRoom: ArrayList<UserRoom>
    lateinit var mAdapterRoom: AdapterRoom
    private lateinit var mapa: SupportMapFragment
    private lateinit var posicion: Any
    private lateinit var exitBinding: ExitBinding
    private var  latitud: Double = 0.0
    private var longitud: Double = 0.0

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPrueba1Binding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)

        adminBinding = inflate(LayoutInflater.from(this))

        userViewModel = run {
            ViewModelProviders.of(this).get(UserViewModel::class.java)
        }


//
//            binding.addContactButton.setOnClickListener {
////                addContact()
//            }
//            binding.refrescarroom.setOnRefreshListener {
//                mAdapterRoom.clear()
//                addObserver()
//                binding.refrescarroom.isRefreshing = false
//            }


        addObserver()

        binding.botonregresar.setOnClickListener {
            val intent = Intent(this, AdminActivity::class.java)
            adminBinding.bottomAppBar.menu.get(0).isEnabled = true
            adminBinding.bottomAppBar.menu.get(1).isEnabled = true
            adminBinding.bottomAppBar.menu.get(2).isEnabled = true
            startActivity(intent)
            finish()
        }
    }
    @RequiresApi(Build.VERSION_CODES.N)
    private fun addObserver() {
        val observer = Observer<List<UserRoom>> { usuarios ->
            if (usuarios != null) {

                listaRoom = usuarios as ArrayList<UserRoom>
                show()
            }
        }
        userViewModel.users.observe(this, observer)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun show() {

        binding.RecyclerRoom.layoutManager = LinearLayoutManager(this)
        binding.RecyclerRoom.setHasFixedSize(true)
        val adapter = AdapterRoom(listaRoom, this)
        binding.RecyclerRoom.adapter = adapter
        mAdapterRoom = adapter

        binding.RecyclerRoom.onItemClick { recyclerView, position, v ->
            customDialogAdminListBinding = ActivityCustomDialogAdminListBinding.inflate(LayoutInflater.from(this))
            posicion = listaRoom.get(position)

            val dialog = AlertDialog.Builder(this).create()
            val view = customDialogAdminListBinding.root
            mapa = supportFragmentManager.findFragmentById(R.id.mapadialogAdmin) as SupportMapFragment

            var fragmentManager: FragmentManager? = supportFragmentManager

            customDialogAdminListBinding.infoNombre.text = (posicion as UserRoom).nombre
            customDialogAdminListBinding.infoApellido.text = (posicion as UserRoom).apellido
            customDialogAdminListBinding.infoEdad.text = (posicion as UserRoom).edad
            customDialogAdminListBinding.infoSexo.text = (posicion as UserRoom).sexo
            customDialogAdminListBinding.infoTelefono.text = (posicion as UserRoom).telefono

            if ((posicion as UserRoom).nombre == null && (posicion as UserRoom).apellido == null &&
                    (posicion as UserRoom).edad == null && (posicion as UserRoom).sexo == null &&
                    (posicion as UserRoom).telefono == null) {
                customDialogAdminListBinding.layoutInfo.visibility = View.GONE
                customDialogAdminListBinding.layoutVacio.visibility = View.VISIBLE
            }

            if ((posicion as UserRoom).latitude != null && (posicion as UserRoom).longitude != null) {
                 latitud = (posicion as UserRoom).latitude?.trim()!!.toDouble()
                 longitud = (posicion as UserRoom).longitude?.trim()!!.toDouble()
                if (latitud != 0.0 && longitud != 0.0) {
                    var latLng = coordenadas(LatLng(latitud, longitud))
                    if (mapa != null) {
                        mapa.getMapAsync(object : OnMapReadyCallback {
                            override fun onMapReady(googleMap: GoogleMap?) {
                                googleMap?.mapType = GoogleMap.MAP_TYPE_NORMAL
                                googleMap?.uiSettings?.isZoomControlsEnabled = true
                                googleMap?.addMarker(
                                        MarkerOptions()
                                                .position(latLng)
                                                .title((posicion as UserRoom).nombre))
                                googleMap?.moveCamera(
                                        CameraUpdateFactory.newCameraPosition(
                                                CameraPosition.Builder()
                                                        .target(latLng)
                                                        .build()
                                        )
                                )
                            }
                        })
                    }
                } else {
                    customDialogAdminListBinding.layoutMapa.visibility = View.GONE
                }
            } else {
                customDialogAdminListBinding.layoutMapa.visibility = View.GONE
            }

            customDialogAdminListBinding.eliminar.setOnClickListener {
                exitBinding = ExitBinding.inflate(LayoutInflater.from(this))
                val dialogEliminar = AlertDialog.Builder(this).create()
                val view = exitBinding.root
                dialogEliminar.setCancelable(false)
                exitBinding.textExit.text= "Quieres eliminar este usuario?"

                exitBinding.aceptar.text = "Si"
                exitBinding.aceptar.setOnClickListener {
                    eliminar((posicion as UserRoom))
                    dialogEliminar.dismiss()
                    dialog.dismiss()
//                    mAdapter.adapterClick(true)
                    //*fragmentManager?.beginTransaction()?.remove(mapadialogAdmin)?.commit()*//*
                }

                exitBinding.cancelar.setText("No")
                exitBinding.cancelar.setOnClickListener {
                    dialogEliminar.dismiss()
                }

                dialogEliminar.setView(view)
                dialogEliminar.show()
                dialogEliminar.window!!.setBackgroundDrawableResource(android.R.color.transparent)
                dialogEliminar.window!!.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)

            }
            dialog.setView(view)
            dialog.setCancelable(true)
            dialog.setOnDismissListener {
//                mAdapterRoom.adapterClick(true)
//                        supportMapFragment.let { it1 -> fragmentManager?.beginTransaction()?.remove(it1)?.commit() }
                if (mapa != null) {
                    fragmentManager?.beginTransaction()?.remove(mapa)?.commit()
                }
            }
            dialog.show()
            dialog.window!!.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)

        }

    }

    private fun eliminar(usuario : UserRoom) {
        userViewModel.eliminar((usuario.id))

    }

    private fun coordenadas(latLng: LatLng): LatLng {
        return LatLng(latLng.latitude , latLng.longitude)
    }

}



//        private fun addContact() {
//            val id = binding.idEditext.text.toString()
//            val name = binding.fistNameEditText.text.toString()
//            val lastName =
//                if (binding.lastNameEditText.text.toString() != "") binding.lastNameEditText.text.toString()
//                else null
//
////            if (name != "" && phone != "") userViewModel.saveUser(UserRoom(id.toInt(),name,lastName,))
//        }


package com.example.validaremailandpassword.view.Fragment

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.validaremailandpassword.Adapters.AdapterRoom
import com.example.validaremailandpassword.R
import com.example.validaremailandpassword.data.UserRoom
import com.example.validaremailandpassword.databinding.*
import com.example.validaremailandpassword.di.UserViewModel
import com.example.validaremailandpassword.onItemClick
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

private var fragmentBinding: ListRoomFragmentBinding? = null
private val binding get() = fragmentBinding!!
class List_Room : Fragment() {
    private lateinit var userViewModel: UserViewModel
    private lateinit var listaRoom: ArrayList<UserRoom>
    lateinit var mAdapterRoom: AdapterRoom
    private lateinit var customDialogAdminListBinding: ActivityCustomDialogAdminListBinding
    private lateinit var adminBinding: ActivityAdminBinding
    private lateinit var mapa: SupportMapFragment
    private lateinit var posicion: Any
    private lateinit var exitBinding: ExitBinding
    private var  latitud: Double = 0.0
    private var longitud: Double = 0.0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fragmentBinding = ListRoomFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adminBinding = ActivityAdminBinding.inflate(LayoutInflater.from(requireContext()))

        userViewModel = ViewModelProviders.of(requireActivity()).get(UserViewModel::class.java)


        addObserver()
    }
    private fun addObserver() {
        val observer = Observer<List<UserRoom>> { usuarios ->
            if (usuarios != null) {

                listaRoom = usuarios as ArrayList<UserRoom>
                show()
            }
        }
        userViewModel.users.observe(requireActivity(), observer)
    }
    private fun show() {

        binding.RecyclerRoom.layoutManager = LinearLayoutManager(requireContext())
        binding.RecyclerRoom.setHasFixedSize(true)
        val adapter = AdapterRoom(listaRoom, requireContext())
        binding.RecyclerRoom.adapter = adapter
        mAdapterRoom = adapter

        binding.RecyclerRoom.onItemClick { recyclerView, position, v ->
            customDialogAdminListBinding = ActivityCustomDialogAdminListBinding.inflate(LayoutInflater.from(requireContext()))
            posicion = listaRoom.get(position)

            val dialog = AlertDialog.Builder(requireContext()).create()
            val view = customDialogAdminListBinding.root
            mapa = fragmentManager?.findFragmentById(R.id.mapadialogAdmin) as SupportMapFragment

            var fragmentManager: FragmentManager? = getFragmentManager()

            customDialogAdminListBinding.infoNombre.text = (posicion as UserRoom).nombre
            customDialogAdminListBinding.infoApellido.text = (posicion as UserRoom).apellido
            customDialogAdminListBinding.infoEdad.text = (posicion as UserRoom).edad
            customDialogAdminListBinding.infoSexo.text = (posicion as UserRoom).sexo
            customDialogAdminListBinding.infoTelefono.text = (posicion as UserRoom).telefono

            if ((posicion as UserRoom).nombre == null && (posicion as UserRoom).apellido == null &&
                (posicion as UserRoom).edad == null && (posicion as UserRoom).sexo == null &&
                (posicion as UserRoom).telefono == null) {
                customDialogAdminListBinding.layoutInfo.visibility = View.GONE
                customDialogAdminListBinding.layoutVacio.visibility = View.VISIBLE
            }

            if ((posicion as UserRoom).latitude != null && (posicion as UserRoom).longitude != null) {
                latitud = (posicion as UserRoom).latitude?.trim()!!.toDouble()
                longitud = (posicion as UserRoom).longitude?.trim()!!.toDouble()
                if (latitud != 0.0 && longitud != 0.0) {
                    var latLng = coordenadas(LatLng(latitud, longitud))
                    if (mapa != null) {
                        mapa.getMapAsync(object : OnMapReadyCallback {
                            override fun onMapReady(googleMap: GoogleMap?) {
                                googleMap?.mapType = GoogleMap.MAP_TYPE_NORMAL
                                googleMap?.uiSettings?.isZoomControlsEnabled = true
                                googleMap?.addMarker(
                                    MarkerOptions()
                                        .position(latLng)
                                        .title((posicion as UserRoom).nombre))
                                googleMap?.moveCamera(
                                    CameraUpdateFactory.newCameraPosition(
                                        CameraPosition.Builder()
                                            .target(latLng)
                                            .build()
                                    )
                                )
                            }
                        })
                    }
                } else {
                    customDialogAdminListBinding.layoutMapa.visibility = View.GONE
                }
            } else {
                customDialogAdminListBinding.layoutMapa.visibility = View.GONE
            }

            customDialogAdminListBinding.eliminar.setOnClickListener {
                exitBinding = ExitBinding.inflate(LayoutInflater.from(requireContext()))
                val dialogEliminar = AlertDialog.Builder(requireContext()).create()
                val view = exitBinding.root
                dialogEliminar.setCancelable(false)
                exitBinding.textExit.text= "Quieres eliminar este usuario?"

                exitBinding.aceptar.text = "Si"
                exitBinding.aceptar.setOnClickListener {
                    eliminar((posicion as UserRoom))
                    dialogEliminar.dismiss()
                    dialog.dismiss()
//                    mAdapter.adapterClick(true)
                    //*fragmentManager?.beginTransaction()?.remove(mapadialogAdmin)?.commit()*//*
                }

                exitBinding.cancelar.setText("No")
                exitBinding.cancelar.setOnClickListener {
                    dialogEliminar.dismiss()
                }

                dialogEliminar.setView(view)
                dialogEliminar.show()
                dialogEliminar.window!!.setBackgroundDrawableResource(android.R.color.transparent)
                dialogEliminar.window!!.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)

            }
            dialog.setView(view)
            dialog.setCancelable(true)
            dialog.setOnDismissListener {
//                mAdapterRoom.adapterClick(true)
//                        supportMapFragment.let { it1 -> fragmentManager?.beginTransaction()?.remove(it1)?.commit() }
                if (mapa != null) {
                    fragmentManager?.beginTransaction()?.remove(mapa)?.commit()
                }
            }
            dialog.show()
            dialog.window!!.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)

        }

    }
    private fun eliminar(usuario : UserRoom) {
        userViewModel.eliminar((usuario.id))

    }

    private fun coordenadas(latLng: LatLng): LatLng {
        return LatLng(latLng.latitude , latLng.longitude)
    }


}
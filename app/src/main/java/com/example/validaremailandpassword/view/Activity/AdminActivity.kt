 package com.example.validaremailandpassword.view.Activity

import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.example.validaremailandpassword.R
import com.example.validaremailandpassword.Adapters.ViewPagerAdapter
import com.example.validaremailandpassword.databinding.ActivityAdminBinding
import com.example.validaremailandpassword.databinding.ExitBinding
import com.example.validaremailandpassword.view.Fragment.PestMaps
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayoutMediator

class AdminActivity : AppCompatActivity() {
    lateinit var preferences: SharedPreferences
    lateinit var navigation : BottomNavigationView
    private lateinit var adminBinding: ActivityAdminBinding
    private lateinit var exitBinding: ExitBinding
//    private val adapter by lazy { ViewPagerAdapter(this) }

    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adminBinding = ActivityAdminBinding.inflate(layoutInflater)
        setContentView(adminBinding.root)
        navigation = findViewById(R.id.bottomAppBar)

//        adminBinding.pager.adapter = adapter

        adminBinding.bottomAppBar.itemIconTintList = ContextCompat.getColorStateList(this, R.color.colorGrisOscuro)
        adminBinding.bottomAppBar.itemTextColor = ContextCompat.getColorStateList(this , R.color.colorGrisOscuro)

        preferences= getSharedPreferences("SHARED_PREF", Context.MODE_PRIVATE)
        var checkCache = preferences.getBoolean("check" , false)
        var rol = preferences.getString("rolUI", "")

        Log.e("TAG", "activity admin: ----> $checkCache --->$rol")
        adminBinding.bottomAppBar.setOnNavigationItemSelectedListener {it
            when(it.itemId) {
                R.id.page_1 -> {
                    exitBinding = ExitBinding.inflate(LayoutInflater.from(this))
                    adminBinding.bottomAppBar.menu.get(0).isEnabled = false
                    adminBinding.bottomAppBar.menu.get(1).isEnabled = false
                    adminBinding.bottomAppBar.menu.get(2).isEnabled = false
                    adminBinding.bottomAppBar.itemIconTintList = ContextCompat.getColorStateList(this, R.drawable.bottom_navigation_selector)
                    adminBinding.bottomAppBar.itemTextColor = ContextCompat.getColorStateList(this, R.drawable.bottom_navigation_selector)

                    val dialog = AlertDialog.Builder(this).create()
                    val view = exitBinding.root
                    dialog.setCancelable(false)
                    exitBinding.textExit.text= "Quieres cerrar sesion?"
                    exitBinding.aceptar.setText("Si")
                    exitBinding.aceptar.setOnClickListener {
                        val editor: SharedPreferences.Editor = preferences.edit()
                        editor.clear()
                        editor.apply()
                        val intent = Intent(this, MainActivity::class.java)
                        editor.putBoolean("check", isRemembered)
                        editor.apply()
                        startActivity(intent)
                        finish()
                    }

                    exitBinding.cancelar.setText("No")
                    exitBinding.cancelar.setOnClickListener {
                        adminBinding.bottomAppBar.menu.get(0).isEnabled = true
                        adminBinding.bottomAppBar.menu.get(1).isEnabled = true
                        adminBinding.bottomAppBar.menu.get(2).isEnabled = true
                        dialog.dismiss()
                        adminBinding.bottomAppBar.itemIconTintList = ContextCompat.getColorStateList(this, R.color.colorGrisOscuro)
                        adminBinding.bottomAppBar.itemTextColor = ContextCompat.getColorStateList(this , R.color.colorGrisOscuro)
                    }

                    dialog.setView(view)
                    dialog.show()
                    dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
                    dialog.window!!.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)


                    true
                }
                R.id.page_2 -> {
                    adminBinding.bottomAppBar.menu.get(0).isEnabled = false
                    adminBinding.bottomAppBar.menu.get(1).isEnabled = false
                    adminBinding.bottomAppBar.menu.get(2).isEnabled = false
                    adminBinding.bottomAppBar.itemIconTintList = ContextCompat.getColorStateList(this, R.drawable.bottom_navigation_selector)
                    adminBinding.bottomAppBar.itemTextColor = ContextCompat.getColorStateList(this, R.drawable.bottom_navigation_selector)
                    val dato = PestMaps()
                    dato.show(supportFragmentManager, "pestList")
//                    progressBarMap = ProgressDialog(this)
//                    progressBarMap.setCanceledOnTouchOutside(false)
//                    progressBarMap.show()
//                    progressBarMap.setContentView(R.layout.loading)
//                    progressBarMap.window!!.setBackgroundDrawableResource(android.R.color.transparent)
//                    val text = progressBarMap.findViewById<TextView>(R.id.loadingText)
//                    text.text = "Cargando mapa..."
                    true
                }
                R.id.page_3 ->{
                    adminBinding.bottomAppBar.menu.get(0).isEnabled = false
                    adminBinding.bottomAppBar.menu.get(1).isEnabled = false
                    adminBinding.bottomAppBar.menu.get(2).isEnabled = false
                    adminBinding.bottomAppBar.itemIconTintList = ContextCompat.getColorStateList(this, R.drawable.bottom_navigation_selector)
                    adminBinding.bottomAppBar.itemTextColor = ContextCompat.getColorStateList(this, R.drawable.bottom_navigation_selector)
//                    val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment_container) as NavHostFragment
//                    val navController = navHostFragment.navController
//                    val navController = findNavController(R.id.nav_host_fragment)
//                    navController.navigate(R.id.list_Room)
//                    adminBinding.bottomAppBar.menu.get(2).
//                    navigation.findNavController().navigate(R.id.list_Room)


//                    navController.addOnDestinationChangedListener { controller, destination, arguments ->
//                        when(destination.id){
//                            R.id.mainFragment -> {
//
//                            }
//                        }
//
//                    }

//                    navigation.findNavController().navigate(R.id.list_Room)
                    val intent = Intent(this , ActivityPrueba1::class.java)
                    startActivity(intent)
                    finish()
                    true
                }
                else -> false
            }

        }

        //esto es lo del tab para las vistas
//        val tabLayoutMediator = TabLayoutMediator(adminBinding.tabLayout, adminBinding.pager,
//            TabLayoutMediator.TabConfigurationStrategy{ tab, position ->
//            when(position){
//                0 -> {
//                    tab.text ="Usuarios"
////                    tab.setIcon(R.drawable.ic_list)
//                }
//                1 -> {
//                    tab.text = "Estadisticas"
////                    tab.setIcon(R.drawable.ic_graph)
//                }
////                2 -> {
////
//////                    tab.text = "Mapas"
////                    tab.setIcon(R.drawable.ic_map)
////
////                }
//            }
//
//        })
//        tabLayoutMediator.attach()

        /*//inicializar fragment
        var fragment: Fragment = Fragment()

        //al cargar el fragment
        supportFragmentManager.beginTransaction().replace(R.id.mapAdmin, fragment).commit()*/


            }







}
package com.example.validaremailandpassword.view.Fragment

import android.app.ProgressDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.example.validaremailandpassword.R
import com.example.validaremailandpassword.databinding.LoadingBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

import java.lang.RuntimeException

private lateinit var loadingBinding: LoadingBinding
private lateinit var mapa: GoogleMap
private lateinit var mapFragment: SupportMapFragment
private lateinit var marker: LatLng
lateinit var Preferencias: SharedPreferences
var onNumeroAleatorio: CustomDialogFragment.OnFragmentInteractionListener? = null

class CustomDialogFragment():DialogFragment() {
    var ARG_PARAM1: String= "latitud"
    var ARG_PARAM2: String = "longitud"
    var latitudd: Double = 0.0
    var longitudd: Double = 0.0


    companion object {
        const val REQUEST_CODE_LOCATION = 0
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(latitud: Double,longitud:Double)

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {


        var view = inflater.inflate(R.layout.activity_custom_dialog_fragment, container, false)
        progressBarMap = ProgressDialog(requireContext())
        progressBarMap.setCanceledOnTouchOutside(false)
        progressBarMap.show()
        progressBarMap.setContentView(R.layout.loading)
        progressBarMap.window!!.setBackgroundDrawableResource(android.R.color.transparent)

        loadingBinding = LoadingBinding.inflate(LayoutInflater.from(context))
        loadingBinding.loadingText.text = "Cargando mapa..."

//        val window: Window? = dialog?.window
//        window?.setBackgroundDrawableResource(R.drawable.background_dialog)

//        mapFragment = activity?.supportFragmentManager?.findFragmentById(R.id.mapadialog) as SupportMapFragment
//        mapFragment.getMapAsync(this)

        mapFragment = activity?.supportFragmentManager?.findFragmentById(R.id.mapadialog) as SupportMapFragment
        Log.e("TAG", "onCreateView: -->mapa de prueba $mapFragment" )
        //mapa asincrono

        mapFragment.getMapAsync(object : OnMapReadyCallback{
            override fun onMapReady(googleMap: GoogleMap?) {
                googleMap!!.setOnMapClickListener {
                    progressBarMap.dismiss()
                    marker = it
                    val markerOptions = MarkerOptions()
                    markerOptions.position(it)
                    /*markerOptions.position.latitude
                    markerOptions.position.longitude*/
                    markerOptions.title("${it.latitude} : ${it.longitude}")
                    googleMap.clear()
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(it,10f))
                    googleMap.addMarker(markerOptions)

                    latitudd = it.latitude
                    longitudd = it.longitude
                    Log.e("TAG", "onMapReady: ${it.latitude}  y ${it.longitude}")



//                    preferences= getSharedPreferences("", Context.MODE_PRIVATE)

                }



                googleMap.uiSettings.isZoomControlsEnabled = true
//                googleMap.setOnMyLocationButtonClickListener(GoogleMap.OnMyLocationButtonClickListener { true } )

            }


        })







        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener){
            onNumeroAleatorio = context

        }else{
        throw RuntimeException(context.toString())
        }

    }
   fun onButtonPressend(latitud: Double,longitud: Double){
        println("latitud: ${latitud} y la longitud : ${longitud} del fragment onButtonPressd")
        onNumeroAleatorio?.onFragmentInteraction(latitud, longitud)
    }


    override fun onDestroyView() {
        super.onDestroyView()
        progressBarMap.dismiss()
        onButtonPressend(latitudd, longitudd)
        Log.e("TAG", "onDestroyView: ---> ${latitudd} y ${longitudd} " )




//        var intent= Intent(context , GestionUserActivity::class.java)
//        intent.putExtra("key1", marker.latitude)
//        intent.putExtra("key", marker.longitude)
//        startActivity(intent)




//        var prefs = activity?.getSharedPreferences(maps.dato, Context.MODE_PRIVATE)
//        prefs!!.edit().putFloat(maps.latitud.toString(), marker.latitude.toFloat()).apply()
//        prefs!!.edit().putFloat(maps.longitud.toString(), marker.longitude.toFloat()).apply()

        try {
            if (mapFragment != null) {
                var fragmentManager: FragmentManager? = fragmentManager
                fragmentManager?.beginTransaction()?.remove(mapFragment)?.commit()

            }
        } catch (e: IllegalStateException) {

        }




    }
    override fun onDetach(){
        super.onDetach()
        onNumeroAleatorio = null
    }


//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (requestCode == 1){
//            if (resultCode == RESULT_OK){
//                var resultado = data?.getIntExtra("result",0)
//                var resultado1 = data?.getIntExtra("result",0)
//                latitudUser.text = ("" +resultado)
//                longitudUser.text= (""+resultado1)
//            }
//            if (requestCode == RESULT_CANCELED) {
//                latitudUser.text = ("NULL")
//                longitudUser.text= ("NULL")
//            }
//        }
//    }


    /*fun onActivityResult(requestCode: Intent, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val returnIntent = Intent()
        *//*returnIntent.putExtra("result", "${marker.position.latitude}, ${marker.position.longitude}")*//*
        activity?.setResult(Activity.RESULT_OK, returnIntent)
        activity?.finish()
    }*/






}






    /*override fun onDestroy() {
        super.onDestroy()

        mapFragment = childFragmentManager.findFragmentById(R.id.mapadialog) as SupportMapFragment
        childFragmentManager.beginTransaction().remove(mapFragment).commit()

        mapFragment.onDestroy()
        mapFragment.onDestroyView()
    }*/







    // mapa
//    private fun createFragment() {
//        val mapFragment = supportFragmentManager.findFragmentById(R.id.mapadialog) as SupportMapFragment
//        mapFragment.getMapAsync(this)
//    }

//        fun onMapReady(googleMap: GoogleMap) {
//        val map  = googleMap
//        map.mapType = GoogleMap.MAP_TYPE_NORMAL
//        map.uiSettings.isZoomControlsEnabled = true
//    }

//    private fun createMarker() {
//        val coordenadas = LatLng(9.919789, -67.351779)
//        val marker =  MarkerOptions().position(coordenadas).title("Mc Donald's")
//        map.addMarker(marker)
//        map.animateCamera(
//            CameraUpdateFactory.newLatLngZoom(coordenadas, 18f)
//        )
//    }

//    private fun isLocationPermissionGranted() =  ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
//
//    private fun enableLocation(){
//        if(!::map.isInitialized) return
//        if(isLocationPermissionGranted()){
//            if (ActivityCompat.checkSelfPermission(
//                    this,
//                    Manifest.permission.ACCESS_FINE_LOCATION
//                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
//                    this,
//                    Manifest.permission.ACCESS_COARSE_LOCATION
//                ) != PackageManager.PERMISSION_GRANTED
//            ) {
//
//                return
//            }
//            map.isMyLocationEnabled = true
//        }else{
//            requestLocationPermission()
//        }
//
//    }

//    private fun requestLocationPermission(){
//        if (ActivityCompat.shouldShowRequestPermissionRationale(
//                this,
//                Manifest.permission.ACCESS_FINE_LOCATION)){
//            Toast.makeText(this, "los permisos de localización no estan activos", Toast.LENGTH_LONG).show()
//        }else{
//            ActivityCompat.requestPermissions(
//                this,
//                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
//                GestionUserActivity.REQUEST_CODE_LOCATION
//            )
//        }
//    }

//    override fun onRequestPermissionsResult(
//        requestCode: Int,
//        permissions: Array<out String>,
//        grantResults: IntArray
//    ) {
//        when(requestCode){
//            GestionUserActivity.REQUEST_CODE_LOCATION -> if (grantResults.isNotEmpty() && grantResults[0]== PackageManager.PERMISSION_GRANTED){
//                if (ActivityCompat.checkSelfPermission(
//                        this,
//                        Manifest.permission.ACCESS_FINE_LOCATION
//                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
//                        this,
//                        Manifest.permission.ACCESS_COARSE_LOCATION
//                    ) != PackageManager.PERMISSION_GRANTED
//                ) {
//
//                    return
//                }
//                map.isMyLocationEnabled = true
//            }else{
//                Toast.makeText(this, "Para activar la localización, ve ajustes y acepta los permisos", Toast.LENGTH_LONG).show()
//            }
//            else -> {}
//        }
//    }

//    override fun onResumeFragments() {
//        super.onResumeFragments()
//        if(!::map.isInitialized) return
//        if(!isLocationPermissionGranted()){
//            if (ActivityCompat.checkSelfPermission(
//                    this,
//                    Manifest.permission.ACCESS_FINE_LOCATION
//                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
//                    this,
//                    Manifest.permission.ACCESS_COARSE_LOCATION
//                ) != PackageManager.PERMISSION_GRANTED
//            ) {
//
//                return
//            }
//            map.isMyLocationEnabled = false
//            Toast.makeText(this, "Para activar la localización, ve ajustes y acepta los permisos", Toast.LENGTH_LONG).show()
//
//        }
//    }

//    override fun onMyLocationButtonClick(): Boolean {
//        return false
//    }
//}
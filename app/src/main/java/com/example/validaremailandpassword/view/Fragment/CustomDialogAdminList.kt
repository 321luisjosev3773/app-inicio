package com.example.validaremailandpassword.view.Fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.example.validaremailandpassword.R
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment

private lateinit var mapa: GoogleMap
private lateinit var mapFragment: SupportMapFragment

class CustomDialogAdminList: DialogFragment(), OnMapReadyCallback {
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        var view = inflater.inflate(R.layout.activity_custom_dialog_admin_list,
                container, false)

        val window: Window? = dialog?.window
        window?.setBackgroundDrawableResource(R.drawable.background_dialog)

        mapFragment = activity?.supportFragmentManager?.findFragmentById(R.id.mapadialogAdmin) as SupportMapFragment
        mapFragment.getMapAsync(this)
        return view
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        val map  = googleMap
        map!!.mapType = GoogleMap.MAP_TYPE_NORMAL
        map.uiSettings.isZoomControlsEnabled = true
    }

    override fun onDestroyView() {
        super.onDestroyView()


        try {
            if (mapFragment != null) {
                var fragmentManager: FragmentManager? = fragmentManager
                fragmentManager?.beginTransaction()?.remove(mapFragment)?.commit()

            }
        } catch (e: IllegalStateException) {

        }


    }

}
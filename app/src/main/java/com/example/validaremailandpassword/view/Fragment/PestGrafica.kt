package com.example.validaremailandpassword.view.Fragment

import android.app.ProgressDialog
import android.content.SharedPreferences
import android.graphics.Color
import android.icu.text.DecimalFormat
import android.icu.text.DecimalFormatSymbols
import android.icu.text.SimpleDateFormat
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.example.validaremailandpassword.model.ListItems
import com.example.validaremailandpassword.R
import com.example.validaremailandpassword.Api
import com.example.validaremailandpassword.databinding.FragmentPestGraficaBinding
import com.example.validaremailandpassword.databinding.LoadingBinding
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import kotlin.collections.ArrayList

private lateinit var loadingBinding: LoadingBinding
private var pestGraficaBinding: FragmentPestGraficaBinding? = null
private val binding get() = pestGraficaBinding!!

class PestGrafica : Fragment() {
    lateinit var progressBarGrafica: ProgressDialog
    var mas : Int = 0
    var fem : Int = 0

    val dataVals = ArrayList<BarEntry>()
    val sexoValues =  ArrayList<PieEntry>()
    var tamañoPrimera : Int = 0
    var tamañoSegunda : Int = 0
    var tamañoTercera : Int = 0
    lateinit var preferences: SharedPreferences


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        pestGraficaBinding = FragmentPestGraficaBinding.inflate(inflater, container, false)

        binding.piechart.visibility = View.GONE
        binding.barchart.visibility = View.GONE

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)




        var lista = listOf("Filtre por sexos o edades" , "sexos", "edades")
        val adapterA = ArrayAdapter<String>(
            requireContext(),
                R.layout.color_spinner,
            lista
        )
        adapterA.setDropDownViewResource(R.layout.spinner_dropdown_item)

        binding.spinner.setAdapter(adapterA)

        binding.spinner.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            @RequiresApi(Build.VERSION_CODES.KITKAT)
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {

                val item = parent?.getItemAtPosition(position).toString()

                if (item.equals("Filtre por sexos o edades")) {
                    binding.piechart.visibility = View.GONE
                    binding.barchart.visibility = View.GONE
                } else if (item.equals("sexos")) {
                    /*pieChart.visibility = View.VISIBLE*/
                    binding.barchart.visibility = View.GONE
                    fem = 0
                    mas = 0
                    tamañoPrimera = 0
                    tamañoSegunda = 0
                    tamañoTercera = 0
                    dataVals.clear()
                    sexoValues.clear()
                    graficaSexos()
                } else if (item.equals("edades")) {
                    binding.piechart.visibility = View.GONE
                    binding.barchart.visibility = View.VISIBLE
                    fem = 0
                    mas = 0
                    tamañoPrimera = 0
                    tamañoSegunda = 0
                    tamañoTercera = 0
                    dataVals.clear()
                    sexoValues.clear()
                    GraficaEdades()
                }

               /* if (item.equals("sexos")) {
                    pieChart.visibility = View.VISIBLE
                    barChart.visibility = View.GONE
                    fem = 0
                    mas = 0
                    tamañoPrimera = 0
                    tamañoSegunda = 0
                    tamañoTercera = 0
                    dataVals.clear()
                    sexoValues.clear()
                    graficaSexos()

                } else {
                    pieChart.visibility = View.GONE
                    barChart.visibility = View.VISIBLE
                    fem = 0
                    mas = 0
                    tamañoPrimera = 0
                    tamañoSegunda = 0
                    tamañoTercera = 0
                    sexoValues.clear()
                    GraficaEdades()
                }*/


            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })
//        botonFlotanteGrafica.setOnClickListener {
//            val layoutInflater = LayoutInflater.from(requireContext())
//            val dialog = AlertDialog.Builder(requireContext()).create()
//            val view = layoutInflater.inflate(R.layout.exit, null, false)
//            dialog.setCancelable(false)
//            val texto:TextView = view.findViewById(R.id.textExit)
//            texto.text= "Quieres salir de la aplicación?"
//            val aceptar = view.findViewById<Button>(R.id.aceptar)
//            aceptar.setText("Si")
//            aceptar.setOnClickListener {
//
//                val editor: SharedPreferences.Editor = preferences.edit()
//                editor.clear()
//                editor.apply()
//                val intent = Intent(context,MainActivity::class.java)
//                editor.putBoolean("check", isRemembered)
//                editor.apply()
//                startActivity(intent)
//                requireActivity().finish()
//            }
//            val cancelar = view.findViewById<Button>(R.id.cancelar)
//            cancelar.setText("No")
//            cancelar.setOnClickListener {
//                dialog.dismiss()
//            }
//
//            dialog.setView(view)
//            dialog.show()
//            dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
//            dialog.window!!.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)
//
//
//        }

    }

    private fun GraficaEdades() {
        loadingBinding = LoadingBinding.inflate(LayoutInflater.from(context))
        progressBarGrafica = ProgressDialog(requireContext())
        progressBarGrafica.setCanceledOnTouchOutside(false)
        progressBarGrafica.show()
        progressBarGrafica.setContentView(loadingBinding.root)
        progressBarGrafica.window!!.setBackgroundDrawableResource(android.R.color.transparent)


        loadingBinding.loadingText.text = "Cargando grafica de edades..."
        Log.e("TAG", "GraficaEdades: masculino ${mas} y femenino ${fem}" )

        val  retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://tecnoparaguana.org.ve")
                .build()


        val api = retrofit.create(Api::class.java)

        api.fetchAllUsers().enqueue(object: Callback<List<ListItems>> {
            //llamada a la api
            @RequiresApi(Build.VERSION_CODES.N)
            override fun onResponse(
                    call: Call<List<ListItems>>,
                    response: Response<List<ListItems>>
            ) {
                Log.e("TAG", "onResponse: --> ${response.body()}")
                contarBarras(response.body())
            }

            override fun onFailure(call: Call<List<ListItems>>, t: Throwable) {
                Log.d("TAG", "falla la consulta en la grafica edad")
                progressBarGrafica.dismiss()
                Toast.makeText(context,"No hay conexion a internet", Toast.LENGTH_SHORT).show()
            }


        })




    }



    @RequiresApi(Build.VERSION_CODES.N)
    private fun contarBarras(datos: List<ListItems>?) {
        progressBarGrafica.dismiss()
        var primera:Int = 0
        var segunda:Int = 0
        var tercera:Int = 0
        if (datos != null) {
            for (f in datos) {
                Log.e("TAG", "contar: ${f.edad}")
                if (f.edad != null){
                    var fechaNacDate: Date = SimpleDateFormat("dd/MM/yyyy").parse(f.edad)
                    var fechaActual= Date(System.currentTimeMillis())
                    var diferenciaFechasMili= fechaActual.time -fechaNacDate.time
                    var segundos = diferenciaFechasMili/1000
                    var minutos= segundos/60
                    var horas= minutos/60
                    var dias= horas/24
                    var edad = dias/365
                    if (edad != null) {
                        if (edad >= 0 && edad <= 10) {

                            primera++
//                        edadPrimera.add(f.edad.toFloat())
//                        tamañoPrimera += primera

                        } else if (edad > 10 && edad <= 20) {
                            segunda++
//                        tamañoSegunda += segunda
//                        edadSegunda.add(segunda.toFloat())

                        } else if (edad > 20) {
//
                            tercera++
//                        edadTercera.add(tercera.toFloat())
//                        tamañoTercera += tercera

                        }

                    }
                }





            }
            tamañoPrimera = primera
            tamañoSegunda = segunda
            tamañoTercera = tercera
            Log.e("TAG", "variable 3 -->${tamañoPrimera} , ${tamañoSegunda}, ${tamañoTercera}")
        }

            //            val dataVals = ArrayList<BarEntry>()
//                Math.round((100 * (datoVene / (datoVene + datoExtranj))))
//                .toFloat(), "Venezolano"

            dataVals.add(BarEntry(0f,(Math.round(tamañoPrimera.toDouble() * 100) / 100).toFloat()))
            dataVals.add(BarEntry(1f,(Math.round(tamañoSegunda.toDouble() * 100) / 100).toFloat()))
            dataVals.add(BarEntry(2f,(Math.round(tamañoTercera.toDouble() * 100) / 100).toFloat()))

            val axiss: ArrayList<String> = ArrayList()
            axiss.add("0 a 10 años")
            axiss.add("11 a 20 años")
            axiss.add("20 + años")
            val xAxis: XAxis = binding.barchart.getXAxis()
            xAxis.position = XAxis.XAxisPosition.BOTTOM_INSIDE
            xAxis.setDrawLabels(true)
            xAxis.granularity = 1f
            // xAxis.labelRotationAngle = +90f

            xAxis.valueFormatter = IndexAxisValueFormatter(axiss)
            xAxis.textColor = Color.rgb(128, 128, 128)
//        setValueTextColor(Color.WHITE)
            val barDataSet1 = BarDataSet(dataVals,"Edades")
            barDataSet1.setColors(Color.rgb(247, 118, 17), Color.rgb(129, 202, 30),
                Color.rgb(26, 166, 107))

            val barData = BarData((barDataSet1))

//                mostrarNumero(barData)
            barData.setValueFormatter{ value, entry, dataSetIndex, viewPortHandler ->
                formatTotalbar(value.toString())
            }
            barData.setValueTextColor(Color.rgb(128,128,128))
            barData.addDataSet(barDataSet1)
        binding.barchart.setData(barData)
//            barChart.invalidate()
        binding.barchart.setExtraOffsets(0f, 30f, 0f, 0f)// para los margenes
        binding.barchart.animateXY(1000, 1000)// cuando carga hace un efecto en vertical
        binding.barchart.data.isHighlightEnabled = false// no se que hace
        binding.barchart.setScaleEnabled(false) // no se que hace
        binding.barchart.setGridBackgroundColor(Color.WHITE)// no se para que hace
        binding.barchart.setTouchEnabled(true)

        binding.barchart.getAxisLeft()?.disableGridDashedLine()
        binding.barchart.getAxisLeft()?.setDrawTopYLabelEntry(false)
        binding.barchart.getAxisLeft()?.setDrawGridLines(false)
        binding.barchart.getAxisLeft()?.setDrawAxisLine(false)
        binding.barchart.setBorderWidth(500f)
        binding.barchart.getAxisLeft()?.setDrawLabels(false)
        binding.barchart.getAxisRight()?.setDrawLabels(false)
        binding.barchart.getXAxis()?.setPosition(XAxis.XAxisPosition.BOTTOM)
        binding.barchart.getAxisRight()?.disableGridDashedLine()
        binding.barchart.getAxisRight()?.setDrawTopYLabelEntry(false)
        binding.barchart.getAxisRight()?.setDrawGridLines(false)
        binding.barchart.getAxisRight()?.setDrawAxisLine(false)
        binding.barchart.getXAxis()?.setDrawGridLines(false)
        binding.barchart.getXAxis()?.setDrawAxisLine(false)
        binding.barchart.getXAxis()?.disableGridDashedLine()
        binding.barchart.legend?.isEnabled = false
        binding.barchart.description?.isEnabled = false


    }


    private fun graficaSexos() {
        loadingBinding = LoadingBinding.inflate(LayoutInflater.from(context))
        progressBarGrafica = ProgressDialog(requireContext())
        progressBarGrafica.setCanceledOnTouchOutside(false)
        progressBarGrafica.show()
        progressBarGrafica.setContentView(loadingBinding.root)
        progressBarGrafica.window!!.setBackgroundDrawableResource(android.R.color.transparent)


        loadingBinding.loadingText.text = "Cargando grafica de sexos..."


        val  retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://tecnoparaguana.org.ve")
                .build()


        val api = retrofit.create(Api::class.java)

        api.fetchAllUsers().enqueue(object: Callback<List<ListItems>> {
            //llamada a la api
            @RequiresApi(Build.VERSION_CODES.N)
            override fun onResponse(
                    call: Call<List<ListItems>>,
                    response: Response<List<ListItems>>
            ) {
                Log.e("TAG", "onResponse: --> ${response.body()}")
                contar(response.body())
            }

            override fun onFailure(call: Call<List<ListItems>>, t: Throwable) {
                Log.d("TAG", "falla la consulta en la grafica de sexo")
                Toast.makeText(context,"No hay conexion a internet", Toast.LENGTH_SHORT).show()
                progressBarGrafica.dismiss()
            }


        })

//        val sexoValues =  ArrayList<PieEntry>()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun contar(sexos: List<ListItems>?) {

        progressBarGrafica.dismiss()
        var sexoMasculino = 0
        var sexoFemenino = 0
        Log.e("TAG", "contador de sexos: ${sexoMasculino} , ${sexoFemenino}" )
        if (sexos != null) {
            for (f in sexos){
                Log.e("TAG", "contar: ${f.sexo}")
                if (f.sexo != null) {
                        if (f.sexo.equals("Masculino")) {
                            sexoMasculino++
//                            mas+=sexoMasculino
//                            masculino.add(f.sexo)
//                            Log.e("TAG", "contar: ---> Arreglo masculino $masculino")
                            Log.e("TAG", "contar: --->contador masculino $mas")
                       } else {
                            if (f.sexo.equals("Femenino")) {
                                sexoFemenino++

//                                femenino.add(f.sexo)
//                                Log.e("TAG", "contar: ---> Arreglo femenino $femenino")
                                Log.e("TAG", "contar: ---> contador femenino $fem" )
                            }

                        }

                }
            }
            mas=sexoMasculino
            fem=sexoFemenino
            Log.e("sexos fin del for", "cantidad de sexos: ${mas} , ${fem}" )
        }

        sexoValues.add( PieEntry( Math.round((100 * (mas.toDouble() / (mas.toDouble() + fem.toDouble())))).toFloat(),"Masculinos"))
//
//        Math.round((100 * (datoVene / (datoVene + datoExtranj))))
//                .toFloat(), "Venezolano"
        sexoValues.add( PieEntry(Math.round((100 * (fem.toDouble() / (fem.toDouble() + mas.toDouble())))).toFloat(),"Femeninos"))
//        Math.round((100 * (fem.toDouble() / (fem.toDouble() + mas.toDouble())))).toFloat()
        val sexoDataSet =  PieDataSet(sexoValues, "Sexos")
//        sexoDataSet.sliceSpace = 5f
        sexoDataSet.selectionShift = 5f
        sexoDataSet.setColors(Color.rgb(74,125,229), Color.rgb(98,186,83))



        val sexoData = PieData ((sexoDataSet))
        sexoData.setValueFormatter { value, entry, dataSetIndex, viewPortHandler ->
            formatTotal(value.toString())
        }
        sexoData.setValueTextSize(10f)
        sexoData.setValueTextColor(Color.WHITE)

        binding.piechart.setData(sexoData)
        binding.piechart.setUsePercentValues(true)
        binding.piechart.description.isEnabled = false
        binding.piechart.animateXY(1000, 1000)
        binding.piechart.setExtraOffsets(5f,10f,5f,5f)
        binding.piechart.setTouchEnabled(true)
        binding.piechart.dragDecelerationFrictionCoef = 0.95f
        binding.piechart.isDrawHoleEnabled= true
        binding.piechart.setHoleColor(Color.alpha(1))
        binding.piechart.holeRadius = 10f//Establecer el radio del agujero, el valor predeterminado es
        binding.piechart.transparentCircleRadius = 20f//Establezca el radio del círculo semitransparente como un porcentaje del radio del gráfico circular, el valor predeterminado es 55%
        binding.piechart.legend?.isEnabled = false
        binding.piechart.visibility = View.VISIBLE
        binding.piechart.setDrawEntryLabels(true)//mostrar texto en las barras defaul true

        /*pieChart.visibility = View.VISIBLE*/

    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun formatTotal(nTotall: String?): String? {
        val simb = DecimalFormatSymbols()
        simb.decimalSeparator = '.'
        simb.groupingSeparator = ','
        val formatea = DecimalFormat("#,###.##", simb)
        var retorno = ""
        if (nTotall?.toDouble()!! > 1) {
            retorno = formatea.format(nTotall?.toDouble()) + "%"
        }
        return retorno
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun formatTotalbar(nTotall: String?): String? {
//        var numero =
//        Log.e("TAG", "formatTotalbar: ----> $numero" )
        val simb = DecimalFormatSymbols()
        simb.decimalSeparator = '.'
        simb.groupingSeparator = ','
        val formatea = DecimalFormat("#", simb)
//        var retorno = formatea.format(nTotall?.toDouble())
        var retorno= (Math.round(nTotall!!.toDouble() * 100) / 100)
        //Math.round(retorno.toDouble())
//        retorno.split('.')
//        if (nTotall?.toDouble()!! > 1) {
//            retorno = formatea.format(nTotall?.toDouble())
//        }
        return retorno.toString()
    }

    override fun onDestroy() {
        super.onDestroy()
        pestGraficaBinding = null
    }

}


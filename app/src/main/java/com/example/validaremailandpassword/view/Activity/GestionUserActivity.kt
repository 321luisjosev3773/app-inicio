 package com.example.validaremailandpassword.view.Activity

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.validaremailandpassword.ApiInterface
import com.example.validaremailandpassword.R
import com.example.validaremailandpassword.RetrofitInstance
import com.example.validaremailandpassword.databinding.ActivityGestionUserBinding
import com.example.validaremailandpassword.databinding.ActivityGestionUserBinding.*
import com.example.validaremailandpassword.databinding.ExitBinding
import com.example.validaremailandpassword.databinding.LoadingBinding
import com.example.validaremailandpassword.model.*
import com.example.validaremailandpassword.view.Fragment.CustomDialogFragment
import com.example.validaremailandpassword.view.Fragment.DatePickerFragment

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.util.*
import javax.security.auth.callback.Callback
 enum class ProviderType{
     BASIC,
     GOOGLE
 }
 @SuppressLint("StaticFieldLeak")
 private lateinit var binding : ActivityGestionUserBinding
 private lateinit var bdMessage : ExitBinding
 private lateinit var bdprogress : LoadingBinding
 class GestionUserActivity : AppCompatActivity(), CustomDialogFragment.OnFragmentInteractionListener {
     lateinit var progressBarGestion: ProgressDialog
     lateinit var preferences: SharedPreferences
     var DATOS: String=""
     var nombree:String =""
     var apellidoo:String=""
     var sexos:String =""
     var telefonoo:String=""
     var edad: String = ""
     var longitudd: String = ""
     var latvieja: String = ""
     var lntvieja: String = ""
     var latitudd: String = ""
     var rolUI: String = "usuario"
     var sexoViejo = ""
     var sexoNuevo = ""
//     lateinit var idd: Any
     var idd :Int?= null
     var emaill: String? = ""
     var passwordd : String? = ""
     var nombreNuevo: String =""
     var nombreViejo: String=""
     var apellidoNuevo : String=""
     var apellidoViejo: String=""
     var telefonoViejo:String=""
     var telefonoNuevo: String=""
     var longitudVieja:String=""
     var longitudNueva: String=""
     var latitudNueva: String=""
     var latitudVieja: String=""
     var booNombre: Boolean = true
     var booApellido: Boolean = true
     var booEdad: Boolean = true
     var booSexo: Boolean = true
     var booTelefono: Boolean = true
     var isRemembered = false
    private var dt : String = ""
//    companion object {
//        const val REQUEST_CODE_LOCATION = 0
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       binding = inflate(layoutInflater)
        setContentView(binding.root)

        //guardar la llave de sesion
        preferences= getSharedPreferences("SHARED_PREF", Context.MODE_PRIVATE)
        var checkCache = preferences.getBoolean("check" , true)
        var idCache = preferences.getInt("id", 0)
        var emailCache = preferences.getString("email", "false")
        var passwordCache = preferences.getString("password" , "")
        nombreViejo  = preferences.getString("nombre", "").toString()
        apellidoViejo = preferences.getString("apellido", "").toString()
        edad = preferences.getString("edad", "").toString()
        var sexoCache = preferences.getString("sexo", "")
        var latitudCache = preferences.getString("latitude", "")
        var longitudCache = preferences.getString("longitude", "")
        telefonoViejo = preferences.getString("telefono", "").toString()
        var rol = preferences.getString("rolUI", "")



        latvieja = latitudCache!!
        lntvieja = longitudCache!!
        longitudVieja = longitudCache!!
        latitudVieja= latitudCache!!

        if (nombreViejo != "")  {
            binding.textNombre.setText(nombreViejo)
            binding.textApellido.setText(apellidoViejo)
            binding.fechaNacimiento.setText(edad)
            if(sexoCache ==  "Masculino"){
                binding.radioButton1.isChecked = true
                sexoViejo = "Masculino"
//                sexo(sexoNuevo,sexoViejo)
            }else{
                binding.radioButton2.isChecked = true
                sexoViejo = "Femenino"
//                sexo(sexoNuevo,sexo)
            }
            binding.textTelefono.setText(telefonoViejo)
            binding.latitudUser.setText(latitudCache)
            binding.longitudUser.setText(longitudCache)
            latitudd = latitudCache
            longitudd = longitudCache

//            ButtonValidarUser.isClickable = false
            binding.ButtonValidarUser.isEnabled = false


        }



        Log.e("TAG", "onCreate: checkbox ${checkCache}" )
        Log.e("TAG", "onCreate: idCache ${idCache}" )
        Log.e("TAG", "onCreate: emailcache ${emailCache}" )
        Log.e("TAG", "onCreate: password ${passwordCache}" )
        Log.e("TAG", "onCreate: nombre ${nombreViejo}" )
        Log.e("TAG", "onCreate: apellido ${apellidoViejo}" )
        Log.e("TAG", "onCreate: edad ${edad}" )
        Log.e("TAG", "onCreate: sexo ${sexoCache}" )
        Log.e("TAG", "onCreate: latitude ${latitudCache}" )
        Log.e("TAG", "onCreate: longitude ${longitudCache}" )
        Log.e("TAG", "onCreate: telefono ${telefonoViejo}" )

        binding.textNombre.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                nombreNuevo = s.toString()
//                validarnombreCache(s.toString(), nombreViejo)
//                validarCache()
                Log.e("TAG", "s: ${s.toString()}" )
                Log.e("TAG", "nombreCache : ${nombreViejo.toString()}" )
                Log.e("TAG", "onTextChanged: validacion ${nombreViejo.toString()!!.equals(s.toString())}")


                Log.e("TAG", "onTextChanged: s tamaño ${s.toString().length}")
                Log.e("TAG", "onTextChanged: nombre cache tamaño ${nombreViejo.toString().length}")
                if (s.toString() != nombreViejo.toString()){
//                    ButtonValidarUser.isEnabled = true
                    booNombre = false
                    validarCache()
                }else if (s.toString() == nombreViejo.toString()){
//                    ButtonValidarUser.isEnabled = false
                    booNombre = true
                    validarCache()
                }

            }

        })
        binding.textApellido.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                apellidoNuevo = s.toString()
//                validarCache()
                if (s.toString() != apellidoViejo.toString()){
//                    ButtonValidarUser.isEnabled = true
                    booApellido = false
                    validarCache()
                }else if (s.toString() == apellidoViejo.toString()){

                    booApellido = true
                    validarCache()
                }
            }
        })
        binding.textTelefono.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                telefonoNuevo = s.toString()
                validarCache()
                Log.e("TAG", "onTextChanged: validacion ${telefonoViejo.toString()!!.equals(s.toString())}")
                Log.e("TAG", "numeroViejo: $telefonoViejo" )
                Log.e("TAG", "numeroNuevo: $s" )
                if (s.toString().trim() != telefonoViejo.toString()){
//                    ButtonValidarUser.isEnabled = true
                    booTelefono = false
                    validarCache()

                }else if (s.toString().trim() == telefonoViejo.toString()){
//                    ButtonValidarUser.isEnabled = false
                    booTelefono = true
                    validarCache()
                }
            }
        })


        idd = idCache
        emaill = emailCache
        passwordd = passwordCache


        binding.emailGestion.text = emaill

        //boton de salir
        binding.exit.setOnClickListener {
            bdMessage = ExitBinding.inflate(LayoutInflater.from(this))

            val dialog = AlertDialog.Builder(this).create()
            val view = bdMessage.root
            dialog.setCancelable(false)

//            bdMessage = ExitBinding.inflate(LayoutInflater.from(this))
//            val text = findViewById<TextView>(R.id.textExit)
            bdMessage.textExit.text = "Quieres salir de la aplicación?"

            bdMessage.aceptar.text= "Si"
            bdMessage.aceptar.setOnClickListener {

                val editor: SharedPreferences.Editor = preferences.edit()
                editor.clear()
                editor.apply()
                val intent = Intent(this, MainActivity::class.java)
                editor.putBoolean("check", com.example.validaremailandpassword.view.Activity.isRemembered)
                editor.apply()
                startActivity(intent)
                finish()
            }

            bdMessage.cancelar.setText("No")
            bdMessage.cancelar.setOnClickListener {
                dialog.dismiss()
            }

            dialog.setView(view)
            dialog.show()
            dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            dialog.window!!.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)

        }

        binding.ButtonedadUser.setOnClickListener { showDatePickerDialog() }


        binding.Buttonir.setOnClickListener {
            showDialogFragment()
        }


        binding.radioButton1.setOnClickListener {
            binding.sexoUser.visibility = View.GONE
            binding.radioButton1.highlightColor = Color.parseColor("#FF6200EE")
            sexoNuevo = "Masculino"
            if (sexoNuevo != sexoViejo){
                booSexo = false
                validarCache()
            }else{
                booSexo = true
                validarCache()
            }
        }
        binding.radioButton2.setOnClickListener {
            binding.sexoUser.visibility = View.GONE
            binding.radioButton2.highlightColor = Color.parseColor("#FF6200EE")
            sexoNuevo = "Femenino"
            if (sexoNuevo != sexoViejo){
                booSexo = false
                validarCache()
            }else{
                booSexo = true
                validarCache()
            }

        }

        binding.ButtonValidarUser.setOnClickListener {
            validate()
        }

        binding.errorUbication.visibility = View.GONE
        binding.sexoUser.visibility = View.GONE

    }



     private fun showDialogFragment(){
        val dato = CustomDialogFragment()
        dato.show(supportFragmentManager, "customDialog")

    }



    // calendario
    private fun showDatePickerDialog() {
        val datePicker = DatePickerFragment { day, month, year -> onDateSelected(day, month, year) }
        datePicker.show(supportFragmentManager, "datePicker")

    }

    private fun onDateSelected(day: Int, month: Int, year: Int) {
        dt ="$day/${month+1}/${year}"
//        var fechaNacDate:Date= SimpleDateFormat("dd/MM/yyyy").parse(dt)
//        var fechaActual= Date(System.currentTimeMillis())
//        var diferenciaFechasMili= fechaActual.time -fechaNacDate.time
//        var segundos = diferenciaFechasMili/1000
//        var minutos= segundos/60
//        var horas= minutos/60
//        var dias= horas/24
//        edad = dias/365
      binding.fechaNacimiento.text = "naciste el día $day, del mes ${month + 1}, en el año $year."
        if (dt != edad){
            booEdad = false
            validarCache()
        }else{
            booEdad = true
            validarCache()
        }

    }



    private fun validarCache(){
       if (booApellido == true && booNombre == true && booEdad == true
               && booSexo == true && booTelefono == true){
           binding.ButtonValidarUser.isEnabled = false
       }else{
           binding.ButtonValidarUser.isEnabled = true
       }
    }


    //    validaciones de los datos
    private fun validate() {
        val result = arrayOf(validateName(), validateLastName(), validateTlf(),validateSexo(),validatefecha(),validateUbicacion())
        if (false in result) {
            return
        } else {
            bdprogress = LoadingBinding.inflate(LayoutInflater.from(this))
            progressBarGestion = ProgressDialog(this)
            progressBarGestion.setCanceledOnTouchOutside(false)
            progressBarGestion.show()
            progressBarGestion.setContentView(bdprogress.root)

            bdprogress.loadingText.text = "Cargando Datos..."

            progressBarGestion.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            Log.e("TAG", "onDateSelected: datos de fecha ${dt}" )
            gestion(nombree,apellidoo,telefonoo, sexos, dt,rolUI, latitudd, longitudd, emaill!!, passwordd!!, idd as Int)
        }
    }

     private fun validateUbicacion(): Boolean {
         return if (binding.latitudUser.text == "" || binding.latitudUser.text == "0.0"){
             binding.errorUbication.visibility = View.VISIBLE
             binding.errorUbication.text = "coloca una ubicación"
             false
         }else{
             true
         }
     }

     private fun validatefecha(): Boolean {
         if (binding.fechaNacimiento.text == "" ){
          binding.fechaNacimiento.text="campo fecha vacio, es requerido"
           return false
        }else if (dt == ""){
             dt = edad
            return true
         } else {
          return true
       }
    }

    private fun validateName(): Boolean {
        val name = binding.nombreUser.editText?.text.toString()
        nombree = name

        Log.e("TAG", "validateName: NOMBRE --> $name")

        return when {
            name.isEmpty() -> {
                binding.nombreUser.error = "*El nombre es requerido"
                false
            }
            !name.matches("^[a-zA-Z]{4,}(?: [a-zA-Z]+){0,2}\$".toRegex()) -> {
                binding.nombreUser.error = "Escribe un nombre valido"
                false
            }
            else -> {
                binding.nombreUser.error = null
                true
            }
        }
    }

    private fun validateLastName(): Boolean {
        val lastName = binding.apellidoUser.editText?.text.toString()
        apellidoo = lastName

        Log.e("TAG", "validateLastName: Apellido --> $lastName")

        return when {
            lastName.isEmpty() -> {
                binding.apellidoUser.error = "*El apellido es requerido"
                false
            }
            !lastName.matches("^[a-zA-Z]{4,}(?: [a-zA-Z]+){0,2}$".toRegex()) -> {
                binding.apellidoUser.error = "Escribe un apellido valido"
                false
            }
            else -> {
                binding.apellidoUser.error = null
                true
            }
        }
    }


    fun validateSexo(): Boolean {
        var id = binding.radioGroup.checkedRadioButtonId

        Log.e("RADIOGROUP--->", "SELECCIONO ${id}" )

        return when(id) {
            binding.radioButton1.id -> {
                sexos = "Masculino"
                Log.e("TAG", "validateSexo: ----> sexo masculino when ${sexos}" )

                true
            }
            binding.radioButton2.id -> {
                sexos = "Femenino"
                Log.e("TAG", "validateSexo: ----> sexo femenino when ${sexos}" )
                true
            }
            else -> {
                binding.sexoUser.visibility = View.VISIBLE
                binding.sexoUser.text="el sexo es requerido"

                false }
        }

    }

    private fun validateTlf(): Boolean {
        val tlf = binding.tlfUser.editText?.text.toString()
        telefonoo = tlf

        return when {
            tlf.isEmpty() -> {
                binding.tlfUser.error = "*El numero de telefono es requerido"
                false
            }
            !tlf.matches("[0-9]\\d{10}".toRegex()) -> {
                binding.tlfUser.error = "Escribe un numero de telefono valido"
                false
            }
            else -> {
                binding.tlfUser.error = null
                true
            }
        }
    }

    private fun gestion(nombre:String, apellido:String, telefono:String, sexo: String, edads:String, rolUI:String, latitud: String, longitud: String
    ,email:String, password:String, id: Int){
        val retGes = RetrofitInstance.getRetrofitInstance().create(ApiInterface::class.java)
        val registerInfo = GestionList(nombre,apellido,telefono,sexo, edads, rolUI,latitud,longitud, email,password)
        val reId = id
        retGes.gestion(reId, registerInfo).enqueue(object : Callback, retrofit2.Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

                progressBarGestion.dismiss()
                Toast.makeText(this@GestionUserActivity,"falla de conexion", Toast.LENGTH_SHORT).show()

            }
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.code() == 200) {
                    progressBarGestion.dismiss()

                    Toast.makeText(this@GestionUserActivity, "Registro exitoso", Toast.LENGTH_SHORT).show()
                    binding.ButtonValidarUser.isEnabled = false
                    guardarDatos()
                    Log.e("TAG", "respuesta gestion: ---> ${response.body()}")

                    val editor  = sharedPreferences.edit()
                    editor.putString("nombre", nombree)
                    editor.putString("apellido", apellidoo)
                    editor.putString("edad", dt)
                    editor.putString("sexo", sexos)
                    editor.putString("latitude", latitudd)
                    editor.putString("longitude", longitudd)
                    editor.putString("telefono", telefonoo)
                    editor.apply()

                }
                else{

                    Toast.makeText(this@GestionUserActivity, "Registro fallido ", Toast.LENGTH_SHORT).show()
                    progressBarGestion.dismiss()
                }
            }
        })
    }

     private fun guardarDatos() {

            nombreViejo = nombree
            apellidoViejo = apellidoo
            edad = dt
            telefonoViejo = telefonoo
            longitudVieja = longitudd
            latitudVieja=latitudd
            sexoViejo = sexos
//              booNombre = true
//              booApellido = true
//              booEdad = true
//              booSexo = true
//              booTelefono = true


     }

     override fun onFragmentInteraction(latitud: Double, longitud: Double) {
         Log.e("ACTIVITY", "ACTIVITY: TEXTVIEW: ${latitud}, ${longitud}" )
         longitudNueva = longitud.toString().trim()
         latitudNueva = latitud.toString().trim()
          latitudd = latitud.toString().trim()
         longitudd = longitud.toString().trim()
         var latitudUser = findViewById<TextView>(R.id.latitudUser)
         var longitudUser= findViewById<TextView>(R.id.longitudUser)
         latitudUser?.text = latitudd.toString()
         longitudUser?.text = longitudd.toString()

//         validate()
//         lanlnt(latitud, longitud)

         if (latitudUser.text != ""){
             binding.errorUbication.visibility = View.GONE

         }

     }

     override fun onPause() {
         super.onPause()

         Log.e("TAG", "onPause: Pausa")
     }






 }









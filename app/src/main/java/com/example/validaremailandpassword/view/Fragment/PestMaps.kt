package com.example.validaremailandpassword.view.Fragment

import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.example.validaremailandpassword.model.ListItems
import com.example.validaremailandpassword.R
import com.example.validaremailandpassword.Api
import com.example.validaremailandpassword.databinding.ActivityAdminBinding
import com.example.validaremailandpassword.databinding.ActivityAdminBinding.inflate
import com.example.validaremailandpassword.databinding.FragmentPestMapsBinding
import com.example.validaremailandpassword.view.Activity.AdminActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private lateinit var mapFragmentAdmin: SupportMapFragment
private lateinit var latitudesUser: ArrayList<String>
private var longitud:Double= 0.0
private var latitud :Double= 0.0
private var correos: String = ""
lateinit var progressBarMap: ProgressDialog

private  var latlngArray = ArrayList<LatLng>()
private  var correosArray = ArrayList<String>()
private lateinit var activity2: AdminActivity
private var pestMapsBinding:FragmentPestMapsBinding? = null
private val binding get() = pestMapsBinding!!
class PestMaps : DialogFragment() {
    private lateinit var latLng: LatLng

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        activity2 = activity as AdminActivity

//        adminBinding = inflate(LayoutInflater.from(requireContext()))
        //inicializar vista
        pestMapsBinding = FragmentPestMapsBinding.inflate(inflater, container, false)
        /*mapFragmentAdmin  = activity?.supportFragmentManager?.findFragmentById(R.id.mapadialog) as SupportMapFragment*/
        /*mapFragmentAdmin?.getMapAsync(callback0)*/
//        animation("x")


        mapFragmentAdmin = childFragmentManager?.findFragmentById(R.id.mapAdmin) as SupportMapFragment
//        mapFragmentAdmin = activity?.supportFragmentManager?.findFragmentById(R.id.mapAdmin) as SupportMapFragment
        obtenerDato()

        return binding.root

    }


    private fun obtenerDato() {


//        Log.e("TAG", "cargar mensaje: ------> hola" )
        val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://tecnoparaguana.org.ve")
                .build()

        val api = retrofit.create(Api::class.java)

        api.fetchAllUsers().enqueue(object : Callback<List<ListItems>> {
            //llamada a la api
            override fun onResponse(
                    call: Call<List<ListItems>>,
                    response: Response<List<ListItems>>
            ) {
//                Log.e("TAG", "onResponse: --> ${response.body()}")


                if (response.body() != null) {
                //latitud
//               for (f in response.body()!!) {
//                Log.e("TAG", "latitud del los usuarios: ${f.latitude} ${f.longitude}")
//                    if (f.latitude.trim().toDouble() > 0 && f.longitude.trim().toDouble() > 0) {
//                        latitud = f.latitude.trim().toDouble()
//                        longitud = f.longitude.trim().toDouble()
//                        latLng = coordenadas(LatLng(latitud, longitud))
//                        Log.e("GEOLOCALIZACION", "GEO----> ${latLng}")
//                        latlngArray.add(latLng)
//                    }
//                }


                for (i in response.body()!!) {
                    /*Log.e("TAG", "onResponse: $i")*/
                    /*Log.e("TAG", "latitud del los usuarios: ${i.latitude} ${i.longitude}")*/

                    Log.e("TAG", "latitud del los usuarios: SIN FILTRO ${i.latitude} ${i.longitude}")
                    if (i.latitude != null && i.longitude != null){
                        if (i.latitude.trim().toDouble() != 0.0 && i.longitude.trim().toDouble() != 0.0) {
                            Log.e("TAG", "latitud del los usuarios: CON FILTRO ${i.latitude} ${i.longitude}")
                            correos = i.email
                            latitud = i.latitude.trim().toDouble()
                            longitud = i.longitude.trim().toDouble()
                            latLng = coordenadas(LatLng(latitud, longitud))
                            Log.e("GEOLOCALIZACION", "GEO----> ${latLng}")
                            correosArray.add(correos)
                            latlngArray.add(latLng)
                        }
                    }
                }
                        mapFragmentAdmin?.getMapAsync(callback0)

               }
            }

            override fun onFailure(call: Call<List<ListItems>>, t: Throwable) {
                Log.e("TAG", "Falla de consulta en los mapas ")
                Toast.makeText(context, "Falla al cargar el mapa", Toast.LENGTH_SHORT).show()
//                progressBarMap.dismiss()
//                progressBarMap.visibility = View.INVISIBLE
            }
        })


    }

    private val callback0= OnMapReadyCallback {googleMap ->
        googleMap.mapType = GoogleMap.MAP_TYPE_NORMAL
        googleMap.uiSettings.isZoomControlsEnabled= true
        googleMap.uiSettings.isCompassEnabled = false
        googleMap.uiSettings.isRotateGesturesEnabled = false
        googleMap.uiSettings.isTiltGesturesEnabled = false
        for (i in 0 until latlngArray.size) {
            Log.e("listaaaaa", "MI LISTAAAA--> $latlngArray" )
            googleMap.addMarker(
                    MarkerOptions().position(latlngArray.get(i))
                                   .title("${correosArray.get(i)}")
            )
//            googleMap.setOnInfoWindowClickListener{
//                funcionMensaje(correosArray.get(i))
//            }
            googleMap.moveCamera(
                    CameraUpdateFactory.newCameraPosition(
                            CameraPosition.Builder()
                                    .target(latlngArray.get(i))
//                                    .zoom(10f)
                                    .build()
                    )
            )
        }
    }
//    private fun funcionMensaje(email: String) {
//        val editor : SharedPreferences.Editor = sharedPreferences.edit()
//        editor.clear()
//        editor.apply()
//        var intent= Intent(requireContext() , practicaMapa::class.java)
//        editor.putString("email", email)
//        editor.apply()
//        startActivity(intent)
////        requireActivity().finish()
//    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        var barra = view?.findViewById<ProgressBar>(R.id.progress_horizontalGrafica)
//        progressBarMap = barra
//        progressBarMap.visibility = View.VISIBLE
    }

    private fun coordenadas(LatLng:LatLng):LatLng{
        return LatLng(LatLng.latitude, LatLng.longitude)
    }

    override fun onDestroy() {
        super.onDestroy()

        try {
            if (mapFragmentAdmin != null) {
                activity2.navigation.menu.get(1).isEnabled = true
                activity2.navigation.menu.get(0).isEnabled = true
                activity2.navigation.menu.get(2).isEnabled = true
                activity2.navigation.itemIconTintList = ContextCompat.getColorStateList(requireContext(),R.color.colorGrisOscuro)
                activity2.navigation.itemTextColor = ContextCompat.getColorStateList(requireContext(),R.color.colorGrisOscuro)
//                adminBinding.bottomAppBar.itemIconTintList = ContextCompat.getColorStateList(requireContext(), R.color.colorGrisOscuro)
//                adminBinding.bottomAppBar.itemTextColor = ContextCompat.getColorStateList(requireContext() , R.color.colorGrisOscuro)
                var fragmentManager: FragmentManager? = fragmentManager
                fragmentManager?.beginTransaction()?.remove(mapFragmentAdmin)?.commit()
                pestMapsBinding = null

            }
        } catch (e: IllegalStateException) {
        }

    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }


}
package com.example.validaremailandpassword.view.Fragment

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.icu.text.SimpleDateFormat
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.solver.widgets.ConstraintWidget.VISIBLE
import androidx.constraintlayout.widget.ConstraintSet.GONE
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.validaremailandpassword.Adapters.AdapterLista
import com.example.validaremailandpassword.Adapters.AdapterNewList
import com.example.validaremailandpassword.Api
import com.example.validaremailandpassword.R
import com.example.validaremailandpassword.RetrofitInstance
import com.example.validaremailandpassword.databinding.ActivityCustomDialogAdminListBinding
import com.example.validaremailandpassword.databinding.ExitBinding
import com.example.validaremailandpassword.databinding.FragmentPestListaBinding
import com.example.validaremailandpassword.databinding.LoadingBinding
import com.example.validaremailandpassword.di.ViewModelListFragment
import com.example.validaremailandpassword.model.ListItems
import com.example.validaremailandpassword.onItemClick
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import kotlin.collections.ArrayList
import android.view.View.VISIBLE as VI


private lateinit var loadingBinding: LoadingBinding
private lateinit var customDialogAdminListBinding: ActivityCustomDialogAdminListBinding
private lateinit var exitBinding: ExitBinding
private var pestListaBinding: FragmentPestListaBinding? = null
private val binding get() = pestListaBinding!!
lateinit var viewmodel : ViewModelListFragment

class PestLista : Fragment() {
//    private var mAdapter: AdapterLista? = null
    private var mAdapter : AdapterLista? = null
    private lateinit var supportMapFragment : SupportMapFragment
    lateinit var progressBarLista: ProgressDialog
    private lateinit var listaUser: ArrayList<ListItems>
    private var listaFiltrado = ArrayList<ListItems>()
    private var  latitud: Double = 0.0
    private var longitud: Double = 0.0
    lateinit var latLng: LatLng
    private lateinit var mp:Any
    private var numero = ""
    private var nombree = ""
    private val PHONE_CALL_REQUEST_CODE = 1
    private var lista: ArrayList<ListItems> = ArrayList()
    lateinit var preferences: SharedPreferences
    companion object{
        private const val ARG_OBJET = "object"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewmodel = activity?.let {
            ViewModelProvider(it).get(ViewModelListFragment::class.java) }!!
    }


    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
       pestListaBinding = FragmentPestListaBinding.inflate(inflater, container, false)
        return binding.root
    }


    @SuppressLint("WrongConstant")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        refreshApp()

        // MENSAJE DEL DIALOG DE CARGA -------------------------------------
        loadingBinding = LoadingBinding.inflate(LayoutInflater.from(context))
        progressBarLista = ProgressDialog(requireContext())
        progressBarLista.setCanceledOnTouchOutside(false)
        progressBarLista.show()
        progressBarLista.setContentView(loadingBinding.root)
        loadingBinding.loadingText.text = "Cargando lista..."
        progressBarLista.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        // FIN DEL MENSAJE DEL DIALOG DE CARGA -------------------------------------

        binding.Recycler.layoutManager = LinearLayoutManager(requireContext())


        preferences = activity?.getSharedPreferences("SHARED_PREF", Context.MODE_PRIVATE)!!
        var checkCache = preferences.getBoolean("check", false)
        var rol = preferences.getString("rolUI", "")
        Log.e("TAG", "lista check ----> $checkCache rol -->$rol")

        // CODIGO COMENTADO PRUEBA COROUTINES



        val  retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://tecnoparaguana.org.ve")
                .build()


        val api = retrofit.create(Api::class.java)

            api.fetchAllUsers().enqueue(object : Callback<List<ListItems>> {
                //llamada a la api
                @RequiresApi(Build.VERSION_CODES.N)
                override fun onResponse(
                        call: Call<List<ListItems>>,
                        response: Response<List<ListItems>>
                ) {
//                Log.e("TAG", "onResponse: --> ${response.body()}")
                    listaUser = (response.body() as ArrayList<ListItems>?)!!
                    showData()
                    progressBarLista.dismiss()
                }

                override fun onFailure(call: Call<List<ListItems>>, t: Throwable) {
                    Log.e("TAG", "Falla de consulta en la lista")
                    Toast.makeText(context, "Falla de consulta en la lista", Toast.LENGTH_SHORT).show()
                    progressBarLista.dismiss()

//                refreshApp()
                    binding.swipeToRefresh.setOnRefreshListener {
                        mAdapter?.clear()
                        refres(lista)
                        binding.swipeToRefresh.isRefreshing = false
                    }
                    binding.buttonRefres.visibility = VISIBLE
                    binding.buttonRefres.setOnClickListener {
                        loadingBinding = LoadingBinding.inflate(LayoutInflater.from(context))
                        progressBarLista = ProgressDialog(requireContext())
                        progressBarLista.setCanceledOnTouchOutside(false)
                        progressBarLista.show()
                        progressBarLista.setContentView(loadingBinding.root)

                        loadingBinding.loadingText.text = "Cargando lista..."
                        progressBarLista.window!!.setBackgroundDrawableResource(android.R.color.transparent)
                        refres(lista)
                    }
                }


            })

        // FIN DE CODIGO COMENTADO PRUEBA COROUTINES

        // CODIGO DE PRUEBA VIEWMODEL -----------------------------------------

//        mAdapter = AdapterNewList()
//        binding.Recycler.layoutManager = LinearLayoutManager(context)
//        binding.Recycler.adapter = mAdapter
//
//        //observer
//        viewmodel.listState.observe(viewLifecycleOwner){
//            mAdapter?.setItems(list = it)
//
//        }
//
//
//        viewmodel.fetchListItems()

        // --------------------------------------------------------------------------


//        search_user = requireView().findViewById(R.id.searchView)
//        search_user.setQueryHint("Buscar Usuario")
        binding.searchView.setSubmitButtonEnabled(false)
        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {


            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }


            override fun onQueryTextChange(newText: String): Boolean {
                if (newText!!.isNotEmpty()) {
                    var busqueda = newText.toLowerCase()
                    listaFiltrado.clear()

                    lista.forEach {
                        if (it.nombre != null) {
                            if (it.email.toLowerCase(Locale.getDefault()).contains(busqueda) || it.nombre.toLowerCase(Locale.getDefault()).contains(busqueda)) {

                                Log.e("TAG", "onQueryTextChange: ${busqueda}")
                                listaFiltrado.addAll(listOf(it))
                                Log.e("TAG", "onQueryTextChange: ${listaFiltrado}")
                                /*mAdapter.clear()*/
//                            mAdapter.notifyDataSetChanged()
                                val adapter = AdapterLista(listaFiltrado as ArrayList<ListItems>, requireContext())
                                mAdapter = adapter
                                binding.Recycler.adapter = mAdapter

                                /*Log.e("TAG", "onQueryTextChange: ${listaFiltrado.size}" )
                                mAdapter.addRowItem(listaFiltrado)*/

                            } else {
//                            listaFiltrado.clear()
                                /*mAdapter.addRowItem(listaFiltrado)*/
                                val adapter = AdapterLista(listaFiltrado as ArrayList<ListItems>, requireContext())
                                mAdapter = adapter
                                binding.Recycler.adapter = mAdapter
                            }
                        }

                    }

                } else {
                    listaFiltrado.clear()
//                    mAdapter.clear()
//                    mAdapter.addRowItem(listaUser)
                    val adapter = AdapterLista(lista as ArrayList<ListItems>, requireContext())
                    mAdapter = adapter
                    binding.Recycler.adapter = mAdapter
                }
                return true
            }

        })


    }
    private fun refreshApp() {

        binding.swipeToRefresh.setOnRefreshListener {
            mAdapter?.clear()
            refres(lista)
            binding.swipeToRefresh.isRefreshing = false
        }
    }


    //funcion para pasar la informacion de la api al recycler
    @RequiresApi(Build.VERSION_CODES.N)
    private fun showData() {

        for(i in listaUser) {
            if (i.rolUI.equals("usuario")) {
                lista.add(i)
            }
        }


        binding.Recycler.layoutManager= LinearLayoutManager(requireContext())
        binding.Recycler.setHasFixedSize(true)
        val adapter = AdapterLista(lista, requireContext())
        binding.Recycler.adapter = adapter
        mAdapter = adapter



        if(binding.Recycler!= null) {
            binding.Recycler.onItemClick { recyclerView, position, v ->
                if (lista.size != 0){

                    if (mAdapter!!.clickAdapter){
//                            desactivarRecycler.visibility = View.VISIBLE
                        mAdapter!!.adapterClick(false)
                        if (listaFiltrado.size == 0){
                            mp = lista.get(position)
                        }else{
                            var posicion = lista.indexOf(
                                    listaFiltrado.get(position)
                            )
                            mp = lista.get(posicion)
                        }

                        Log.e("TAG", "position del user : " + position)
                        customDialogAdminListBinding = ActivityCustomDialogAdminListBinding.inflate(
                                LayoutInflater.from(context))
                        val layoutInflater = LayoutInflater.from(requireContext())
                        val dialog = AlertDialog.Builder(requireContext()).create()
                        val view = customDialogAdminListBinding.root
                        val mapa = activity?.supportFragmentManager?.findFragmentById(R.id.mapadialogAdmin) as SupportMapFragment
                        //*childFragmentManager?.findFragmentById(R.id.mapadialogAdmin) as SupportMapFragment*//*
//                    supportMapFragment = mapa!!


                        var fragmentManager: FragmentManager? = fragmentManager


                        if ((mp as ListItems).nombre == null){
                            customDialogAdminListBinding.layoutNombre.visibility = View.INVISIBLE
                            customDialogAdminListBinding.layoutNombre.visibility = View.GONE
                        }else{
                            customDialogAdminListBinding.infoNombre.text= (mp as ListItems).nombre
                            nombree = (mp as ListItems).nombre
                        }

                        if ((mp as ListItems).apellido == null){
                            customDialogAdminListBinding.layoutApellido.visibility = View.INVISIBLE
                            customDialogAdminListBinding.layoutApellido.visibility = View.GONE
                        }else{
                            customDialogAdminListBinding.infoApellido.text = (mp as ListItems).apellido
                        }
                        if ((mp as ListItems).edad == null){
                            customDialogAdminListBinding.layoutEdad.visibility = View.INVISIBLE
                            customDialogAdminListBinding.layoutEdad.visibility = View.GONE
                        }else{
                            var fechaNacDate:Date= SimpleDateFormat("dd/MM/yyyy").parse((mp as ListItems).edad)
                            var fechaActual= Date(System.currentTimeMillis())
                            var diferenciaFechasMili= fechaActual.time -fechaNacDate.time
                            var segundos = diferenciaFechasMili/1000
                            var minutos= segundos/60
                            var horas= minutos/60
                            var dias= horas/24
                            var edad2 = dias/365
                            customDialogAdminListBinding.infoEdad.text = edad2.toString()
//                                edad.text = (mp as ListItems).edad.toString()
                        }
                        if ((mp as ListItems).sexo == null){
                            customDialogAdminListBinding.layoutSexo.visibility = View.INVISIBLE
                            customDialogAdminListBinding.layoutSexo.visibility = View.GONE
                        }else{
                            customDialogAdminListBinding.infoSexo.text = (mp as ListItems).sexo
                        }
                        if ((mp as ListItems).telefono == null){
                            customDialogAdminListBinding.layoutTelefono.visibility = View.INVISIBLE
                            customDialogAdminListBinding.layoutTelefono.visibility = View.GONE
                        }else{
                            customDialogAdminListBinding.infoTelefono.text = (mp as ListItems).telefono
                        }
                        if ((mp as ListItems).nombre == null && (mp as ListItems).apellido == null &&
                                (mp as ListItems).edad == null &&  (mp as ListItems).sexo == null &&
                                (mp as ListItems).telefono == null ){
                            customDialogAdminListBinding.layoutInfo.visibility = View.GONE
                            customDialogAdminListBinding.layoutVacio.visibility = VI

                        }


                        if((mp as ListItems).latitude != null && (mp as ListItems).longitude != null){
                            latitud = (mp as ListItems).latitude.trim().toDouble()
                            longitud = (mp as ListItems).longitude.trim().toDouble()
                            if (latitud != 0.0 && longitud != 0.0){
                                latLng = coordenadas(LatLng(latitud!!, longitud!!))

                                Log.e("TAG", "showData: ----este es el mapa ${mapa}")
                                if (mapa != null) {
                                    Log.e("TAG", "showData: ---> entro en el if si ${latLng}")
                                    mapa.getMapAsync(object : OnMapReadyCallback {
                                        override fun onMapReady(googleMap: GoogleMap?) {

                                            Log.e("TAG", "showData: ${latLng}")
                                            Log.e("TAG", "showData: ${latitud}, ${longitud}")

                                            googleMap?.mapType = GoogleMap.MAP_TYPE_NORMAL
                                            googleMap?.uiSettings?.isZoomControlsEnabled = true
                                            googleMap?.addMarker(
                                                    MarkerOptions()
                                                            .position(latLng)
                                                            .title(nombree)
                                            )
                                            googleMap?.moveCamera(
                                                    CameraUpdateFactory.newCameraPosition(
                                                            CameraPosition.Builder()
                                                                    .target(latLng)
//                                                                    .zoom(15f)
                                                                    .build()
                                                    )
                                            )

                                        }

                                    })


                                }
                            }else{
                                customDialogAdminListBinding.layoutMapa.visibility = View.GONE
                            }
                        }else{
                            customDialogAdminListBinding.layoutMapa.visibility = View.GONE
                        }




//                    numero = users[position].telefono
//                    Log.e("TAG", "showData: numero para llamar ---->${numero}")
                        customDialogAdminListBinding.layoutTelefono.setOnClickListener {
                            llamar()
                        }

//                    Log.e("TAG", "showData: ---> ${users[position]}")

                        customDialogAdminListBinding.eliminar.setOnClickListener {
                            exitBinding = ExitBinding.inflate(LayoutInflater.from(context))
                            val layoutInflater = LayoutInflater.from(requireContext())
                            val dialogEliminar = AlertDialog.Builder(requireContext()).create()
                            val view = exitBinding.root
                            dialogEliminar.setCancelable(false)
                            exitBinding.textExit.text= "Quieres eliminar este usuario?"

                            exitBinding.aceptar.setText("Si")
                            exitBinding.aceptar.setOnClickListener {
                                eliminar((mp as ListItems).id, (mp as ListItems).rolUI)
                                dialogEliminar.dismiss()
                                dialog.dismiss()
//                                desactivarRecycler.visibility = View.GONE
                                binding.searchView.setQuery("", false)
                                binding.searchView.clearFocus()
                                mAdapter!!.adapterClick(true)
                                //*fragmentManager?.beginTransaction()?.remove(mapadialogAdmin)?.commit()*//*
                            }

                            exitBinding.cancelar.setText("No")
                            exitBinding.cancelar.setOnClickListener {
                                dialogEliminar.dismiss()
                            }

                            dialogEliminar.setView(view)
                            dialogEliminar.show()
                            dialogEliminar.window!!.setBackgroundDrawableResource(android.R.color.transparent)
                            dialogEliminar.window!!.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)

                        }

                        dialog.setView(view)
                        dialog.setCancelable(true)
                        dialog.setOnDismissListener {
                            mAdapter!!.adapterClick(true)
//                        supportMapFragment.let { it1 -> fragmentManager?.beginTransaction()?.remove(it1)?.commit() }
                            if (mapa != null) {
                                fragmentManager?.beginTransaction()?.remove(mapa)?.commit()

                            }

                        }
                        dialog.show()
                        dialog.window!!.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)

                    }


                }





            }
        }

    }


    private fun coordenadas(LatLng: LatLng):LatLng{
        return LatLng(LatLng.latitude, LatLng.longitude)
    }

    private val miMapa = OnMapReadyCallback { googleMap ->
        Log.e("TAG", "showData: ---> entro en miMapa")

        googleMap.mapType = GoogleMap.MAP_TYPE_NORMAL
        googleMap.uiSettings.isZoomControlsEnabled = true
        googleMap.addMarker(
                MarkerOptions()
                        .position(latLng)
                        .title(nombree)
        )
        googleMap.moveCamera(
                CameraUpdateFactory.newCameraPosition(
                        CameraPosition.Builder()
                                .target(latLng)
                                .zoom(15f)
                                .build()
                )
        )


    }

    private fun llamar() {

        val numeroText = numero.trim()
        val intent = Intent(Intent.ACTION_CALL)
        intent.data = Uri.parse("tel:$numeroText")
        if(ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED){
            startActivity(intent)
        }else{
            ActivityCompat.requestPermissions((context as Activity?)!!, arrayOf(Manifest.permission.CALL_PHONE), PHONE_CALL_REQUEST_CODE)
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PHONE_CALL_REQUEST_CODE){
            if (grantResults.isNotEmpty() && grantResults[0] ==PackageManager.PERMISSION_GRANTED){
                llamar()
            }else{
                Toast.makeText(context, "Debes dar al permiso para hacer la llamada", Toast.LENGTH_LONG).show()
            }
        }
    }


    private fun eliminar(id: Int, rol: String) {
        progressBarLista.dismiss()

        if(rol == "usuario"){

            val api = RetrofitInstance.getRetrofitInstance().create(Api::class.java)

            api.eliminarUser(id).enqueue(object : Callback<ResponseBody> {
                //llamada a la api
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    Log.e("TAG", "Respuesta de eliminacion: --> ${response.body()}")
                    mAdapter!!.clear()
                    refres(lista)
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    Log.d("TAG", "CONSULTA FALLIDA")
                    Toast.makeText(context, "Fallo en conexion a internet", Toast.LENGTH_LONG).show()
                    refreshApp()
                }


            })
        }else{
            progressBarLista = ProgressDialog(requireContext())
            progressBarLista.setCanceledOnTouchOutside(true)
            progressBarLista.show()
            progressBarLista.setContentView(R.layout.eliminar_user_msj)

//            val text = progressBarLista.findViewById<TextView>(R.id.msjEliminartext)
//            text.text = "Cargando lista..."
        }

    }


    fun refres(lista1: ArrayList<ListItems>) {

        lista1.clear()

        // CODIGO COMENTADO PRUEBA COROUTINES --------------------------------------------------

        val  retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://tecnoparaguana.org.ve")
                .build()

        val api = retrofit.create(Api::class.java)

        api.fetchAllUsers().enqueue(object : Callback<List<ListItems>> {
            //llamada a la api
            @SuppressLint("WrongConstant")
            @RequiresApi(Build.VERSION_CODES.N)
            override fun onResponse(
                    call: Call<List<ListItems>>,
                    response: Response<List<ListItems>>
            ) {
                Log.e("TAG", "onResponse: --> ${response.body()}")
                binding.buttonRefres.setVisibility(GONE)
                Toast.makeText(context, "Lista actualizada", Toast.LENGTH_SHORT).show()
                progressBarLista.dismiss()
                listaUser = (response.body() as ArrayList<ListItems>?)!!
                showData()

            }

            override fun onFailure(call: Call<List<ListItems>>, t: Throwable) {
                Log.e("TAG", "Falla de consulta en la lista al refrescar")
                Toast.makeText(context, "Fallo de conexion", Toast.LENGTH_SHORT).show()
                progressBarLista.dismiss()
                refreshApp()
            }


        })

        // FIN DE CODIGO COMENTADO PRUEBA COROUTINES ----------------------------------------------
    }

    override fun onDestroy() {
        super.onDestroy()
        pestListaBinding = null
    }



}



package com.example.validaremailandpassword.Repository

import com.example.validaremailandpassword.Api
import com.example.validaremailandpassword.RetrofitInstance

class RepositoryFragmentList {

    private var apiServise: Api? = null

    init {
        apiServise = RetrofitInstance.getRetrofitInstance().create(Api::class.java)
    }

    suspend fun getlistFragment()= apiServise?.fetchAllUsers2()
}
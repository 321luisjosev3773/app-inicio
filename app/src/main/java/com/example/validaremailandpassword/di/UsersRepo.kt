package com.example.validaremailandpassword.di

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.validaremailandpassword.data.ApiRoom
import com.example.validaremailandpassword.data.UsersDataBase
import com.example.validaremailandpassword.data.UserRoom


@Suppress("DEPRECATION")
class UsersRepo(application: Application) {
    private val apiRoom: ApiRoom? = UsersDataBase.getInstances(application)?.apiroom()

    fun insert(userroom: UserRoom) {
        if (apiRoom != null) InsertAsyncTask(apiRoom).execute(userroom)
    }

    fun getUsers(): LiveData<List<UserRoom>> {
        return apiRoom?.obtenerRoom() ?: MutableLiveData<List<UserRoom>>()
    }

    fun delete(id: Int){
        apiRoom?.eliminar(id)
    }

    @Suppress("DEPRECATION")
    private class InsertAsyncTask(private val apiRoom: ApiRoom) :
            AsyncTask<UserRoom, Void, Void>() {
        override fun doInBackground(vararg userrooms: UserRoom?): Void? {
            for (userroo in userrooms) {
                if (userroo != null) apiRoom.subir(userroo)
            }
            return null
        }
    }
}
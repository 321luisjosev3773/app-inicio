package com.example.validaremailandpassword.di

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.example.validaremailandpassword.data.UserRoom

class UserViewModel(application: Application) : AndroidViewModel(application) {
    private val repository = UsersRepo(application)

    val users = repository.getUsers()

    fun saveUser(userroom: UserRoom) {
        repository.insert(userroom)
    }

    fun eliminar(id:Int) = repository.delete(id)


}
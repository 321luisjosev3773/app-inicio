package com.example.validaremailandpassword.di

import android.app.Application
import androidx.lifecycle.*
import com.example.validaremailandpassword.Repository.RepositoryFragmentList
import com.example.validaremailandpassword.model.ListItems
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class ViewModelListFragment(app:Application): AndroidViewModel(app), CoroutineScope {

    private val _itemSelected = MutableLiveData<ListItems>()
    var itemDataSelected: ListItems? = null

    private val repository = RepositoryFragmentList()

    private val _listState = MutableLiveData<MutableList<ListItems>>()
    val listState: LiveData<MutableList<ListItems>> = _listState

    lateinit var observerOnCategorySelected: Observer<ListItems>

    private val viewModelJob = Job()
    override val coroutineContext: CoroutineContext
        get() = viewModelJob + Dispatchers.Default

    init {
        initObserver()
    }

    // PARA MOSTRAR
    private fun initObserver() {
        observerOnCategorySelected = Observer { value ->
            value.let {
                _itemSelected.value = it
            }
        }
    }

    fun clearSelection() {
//        _itemSelected.value = null
    }

    // PARA SELECCIONAR UN ITEM ----------------------------------------
    fun setItemSelection(item: ListItems) {
        itemDataSelected = item
    }

    // PARA OBTENER LA LISTA MUTABLE DE UNA COROUTINE ----------------------
    fun fetchListItems() {
//        _progressState.value = true
        viewModelScope.launch {
            val response = repository.getlistFragment()
            response?.body()?.Listas.let { list ->
                _listState.value = list
            }
        }
    }

    // PARA CANCELAR EL PROCESO DE EL VIEWMODEL ---------------------------------
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }


}
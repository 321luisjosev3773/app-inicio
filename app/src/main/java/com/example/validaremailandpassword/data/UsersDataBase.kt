package com.example.validaremailandpassword.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(entities = [UserRoom::class], version = 1)
abstract class UsersDataBase: RoomDatabase(){
    abstract fun apiroom(): ApiRoom

    companion object {
        private const val DATABASE_NAME = "score_database"
        @Volatile
        private var INSTANCE: UsersDataBase? = null

        fun getInstances(context: Context): UsersDataBase? {
            INSTANCE ?: synchronized(this) {
                INSTANCE = Room.databaseBuilder(
                    context.applicationContext,
                    UsersDataBase::class.java,
                    DATABASE_NAME
                ).build()
            }
            return INSTANCE
        }
    }
}
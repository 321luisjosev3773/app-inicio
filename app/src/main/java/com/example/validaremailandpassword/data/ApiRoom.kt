package com.example.validaremailandpassword.data

import androidx.lifecycle.LiveData
import androidx.room.*


@Dao
interface ApiRoom {

    @Query("SELECT * FROM Usuarios ORDER BY Id ASC")// seleccionar todo los usuarios y ordenar por id ascendente
    fun obtenerRoom() : LiveData<List<UserRoom>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)//ignorar repetidos
    fun subir(Users: UserRoom)

//    @Delete
//    fun eliminar(userRoom: UserRoom)

//    @Query("DELETE FROM Usuarios WHERE Id = Id")
//    fun eliminarAll(Id: Int)
    @Query("DELETE FROM Usuarios WHERE id =:id")
     fun eliminar(id: Int)

    @Update
    fun update(Users: UserRoom)

}


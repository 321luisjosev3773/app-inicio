package com.example.validaremailandpassword

import com.example.validaremailandpassword.model.GestionList
import com.example.validaremailandpassword.model.ListItems
import com.example.validaremailandpassword.model.ListaModel
import com.example.validaremailandpassword.model.SignUp
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface Api {

    @GET ("/api-rest/")

     fun fetchAllUsers(): Call<List<ListItems>>

    @GET ("/api-rest/")
    suspend fun fetchAllUsers2(): Response<ListaModel>


//    @POST("/ept_movil/login/?login=alcaldiadepapelon@gmail.com&password=123123&CodVer=5.1&CodDis=WUD9K17220903638")
//    fun login(
//    ): Call<ResponseBody>

    @DELETE("/api-rest/{id}")
    fun eliminarUser(@Path("id") id: Int): Call<ResponseBody>
}


interface ApiInterface {

    @GET("/api-rest/?q=")
     fun getdato(@Query("email")emails:String, @Query("password")passwords:String ): Call<List<ListItems>>

    @GET("/api-rest/?q=")
    fun getEmail(@Query("email")emails:String ): Call<ResponseBody>

    @PUT("/api-rest/{id}")
    fun gestion(@Path("id") id: Int ,@Body info: GestionList): retrofit2.Call<ResponseBody>

    @POST("/api-rest")
    fun registerUser(@Body info: SignUp): retrofit2.Call<ResponseBody>
}

class RetrofitInstance {
    companion object {
        val BASE_URL: String = "http://tecnoparaguana.org.ve/"
//            val BASE_URL = "http://201.249.189.185:8390/"

        val interceptor: HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
            this.level = HttpLoggingInterceptor.Level.BODY
        }

        val client: OkHttpClient = OkHttpClient.Builder().apply {
            this.addInterceptor(interceptor)
        }.build()

        val request = Request.Builder()
                .url(BASE_URL)
                .get()
                .build()
        fun getRetrofitInstance(): Retrofit {

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }

    }
}
package com.example.validaremailandpassword.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "Usuarios")

data class UserRoom (
    @ColumnInfo(name = "Id") val id: Int,
    @ColumnInfo(name = "Nombre") val nombre: String? = null,
    @ColumnInfo(name = "Apellido")val apellido: String?= null,
    @ColumnInfo(name = "Correo")val email: String?,
    @ColumnInfo(name = "Edad")val edad: String?= null,
    @ColumnInfo(name = "Sexo")val sexo: String?= null,
    @ColumnInfo(name = "Telefono")val telefono: String?= null,
    @ColumnInfo(name = "password") val password: String?,
    @ColumnInfo(name = "Latitud")val latitude: String?= null,
    @ColumnInfo(name = "Longitud")val longitude: String?= null,
    @ColumnInfo(name = "Rol")val rolUI: String?,

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id_room")
    var idRoom: Int = 0
) :Serializable

